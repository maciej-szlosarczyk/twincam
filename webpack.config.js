const path = require('path')
const glob = require('glob')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const WebpackAssetsManifest = require('webpack-assets-manifest')
const CompressionPlugin = require('compression-webpack-plugin')

module.exports = (_env, _options) => ({
  optimization: {
    minimizer: [
      new TerserPlugin(),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 30000,
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    },
    usedExports: true
  },
  devtool: 'source-map',
  entry: {
    app: './assets/js/app.js'.concat(glob.sync('./vendor/**/*.js')),
    highlighter: './assets/js/highlighter.js'.concat(glob.sync('./vendor/**/*.js')),
    css: './assets/css/app.scss'
  },
  output: {
    filename: '[name]-bundle-[hash].js',
    path: path.resolve(__dirname, './public/assets/js'),
    publicPath: '/assets/js/'
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              filename: 'css/[name]-[contenthash:8].css',
              chunkFilename: 'css/[name]-[contenthash:8].chunk.css'
            }
          },
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              outputPath: './../css',
              sourceMap: true
            }
          },
          'css-loader'
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: '../fonts',
            publicPath: '/assets/fonts/'
          }
        }]
      }

    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      outputPath: './../css',
      filename: 'css/[name]-[contenthash:8].css',
      chunkFilename: 'css/[name]-[contenthash:8].chunk.css'
    }),
    new CopyWebpackPlugin({
      patterns:
      [{ from: 'assets/static/', to: '[name].[ext]' }]
    }),
    new WebpackAssetsManifest({
      // Options go here
      publicPath: true,
      entrypoints: true
    }),
    new CompressionPlugin({})
  ]
})
