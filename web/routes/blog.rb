# frozen_string_literal: true

require File.expand_path("../../lib/actions", __dir__)
require File.expand_path("../../lib/models/comment", __dir__)

# /blog/ routes
class TwinCam < Roda
  path :posts, "/blog/"
  path :post do |post|
    "/blog/#{post.url_path}"
  end
  path :create_comment_for_post do |post|
    "/blog/#{post.url_path}/comments"
  end

  path :page, &:url_path
  path :posts_previous_page do |current_page|
    "/blog/page/#{current_page - 1}"
  end
  path :posts_next_page do |current_page|
    "/blog/page/#{current_page + 1}"
  end
end

# rubocop:disable Metrics/BlockLength
TwinCam.route("blog") do |r|
  @user_id = session["account_id"]
  @current_page = 1

  # GET /blog
  # GET /blog/
  r.root do
    @result = Actions::Posts::Index.new(
      Features.pagination_step,
      @current_page,
      status: "published"
    ).call
    view "blog/index"
  end

  r.on Integer, Integer, String do |year, month, slug|
    # POST /blog/2018/04/hello-world/comments
    r.post "comments" do
      check_csrf!

      suspected_spam = begin
                         time_to_pass = Time.parse(
                           session["spam_protection_timeout"]
                         )

                         if @user_id
                           false
                         else
                           time_to_pass > Time.now
                         end
                       end

      if suspected_spam
        forbidden
      else
        @comment = Actions::Comments::Create
          .new(r.params)
          .call

        if @comment.valid?
          flash["notice"] = "Your comment was saved. Thank you."
          r.redirect(post_path(@comment.post))
        else
          # TODO: figure out how to test this.
          unprocessable_entity
        end
      end
    end

    # GET /blog/2018/04/hello-world
    r.is do
      spam_timeout = Features.spam_timeout
      session["spam_protection_timeout"] = Time.now + spam_timeout

      params = {year: year, month: month, slug: slug, status: "published"}
      @post = Actions::Posts::Show.new(params).call
      if @post
        @page_title = @post.title
        @comment = Actions::Comments::New.new(@post.id, @user_id).call
        view "blog/show"
      else
        not_found
      end
    end
  end

  # GET "/blog/page/3"
  r.on "page" do
    r.on Integer do |page|
      @current_page = page
      @result = Actions::Posts::Index.new(
        Features.pagination_step,
        @current_page,
        status: "published"
      ).call
      if @result.records.empty?
        not_found
      else
        view "blog/index"
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
