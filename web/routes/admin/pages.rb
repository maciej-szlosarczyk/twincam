# frozen_string_literal: true

require File.expand_path("../../../lib/actions/pages", __dir__)

# /admin/pages routes
class TwinCam < Roda
  path :admin_pages, "/admin/pages"
  path :admin_show_page do |page|
    "/admin/pages/#{page.url_path}"
  end
  path :admin_new_page, "/admin/pages/new"
  path :admin_edit_page do |page|
    "/admin/pages/#{page.url_path}/edit"
  end
  path :admin_update_page do |page|
    "/admin/pages/#{page.url_path}/update"
  end
end

# rubocop:disable Metrics/BlockLength
TwinCam.route("pages") do |r|
  # GET "/admin/pages"
  # GET "/admin/pages/"
  r.root do
    @pages = Actions::Pages::Index.new.call
    view "admin/pages/index"
  end

  # GET "/admin/pages/new"
  r.is "new" do
    @page = Actions::Pages::New.new(@user_id).call
    view "admin/pages/new"
  end

  r.on String do |slug|
    routing_params = {slug: slug}
    @page = Actions::Pages::Show.new(routing_params).call

    # PAGE "/admin/pages/hello-world/update"
    r.post "update" do
      check_csrf!

      updated_page = Actions::Pages::Update
        .new(@page, r.params)
        .call

      if updated_page.errors.empty?
        r.redirect admin_show_page_path(@page)
      else

        unprocessable_entity(@page.errors[:page])
      end
    end

    # GET "/admin/pages/2018/04/hello-world/edit"
    r.get "edit" do
      if @page
        view "admin/pages/edit"
      else
        not_found
      end
    end

    # POST "/admin/pages/2018/04/hello-world/delete"
    r.get "delete" do
      # no-op
    end

    # GET "/admin/pages/2018/04/hello-world"
    r.is do
      if @page
        view "admin/pages/show"
      else
        not_found
      end
    end
  end

  # POST "/admin/pages"
  r.post do
    check_csrf!

    @page = Actions::Pages::Create.new(r.params).call

    if @page.errors.empty?
      r.redirect admin_show_page_path(@page)
    else
      unprocessable_entity(@page.errors[:page])
    end
  end
end
# rubocop:enable Metrics/BlockLength
