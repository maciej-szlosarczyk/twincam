# frozen_string_literal: true

require File.expand_path("../../../lib/actions/comments", __dir__)

# /admin/comments routes
class TwinCam < Roda
  path :admin_delete_comment do |comment|
    "/admin/comments/#{comment.id}/delete"
  end
end

TwinCam.route("comments") do |r|
  r.on String do |comment_id|
    routing_params = {comment_id: comment_id}
    @comment = Actions::Comments::Show.new(routing_params).call

    if @comment
      # POST /admin/comments/1/delete
      r.post "delete" do
        check_csrf!

        post_to_return_to = Actions::Comments::Delete.new(@comment).call

        flash["notice"] = "Comment deleted."
        if post_to_return_to
          r.redirect admin_show_post_path(post_to_return_to)
        else
          not_found
        end
      end
    else
      not_found
    end
  end
end
