# frozen_string_literal: true

require File.expand_path("../../../lib/actions/posts", __dir__)

# /admin/posts routes
class TwinCam < Roda
  path :admin, "/admin"
  path :admin_posts, "/admin/posts"
  path :admin_new_post, "/admin/posts/new"
  path :admin_show_post do |post|
    "/admin/posts/#{post.url_path}"
  end
  path :admin_edit_post do |post|
    "/admin/posts/#{post.url_path}/edit"
  end
  path :admin_update_post do |post|
    "/admin/posts/#{post.url_path}/update"
  end

  path :admin_posts_previous_page do |current_page|
    "/admin/posts/page/#{current_page - 1}"
  end
  path :admin_posts_next_page do |current_page|
    "/admin/posts/page/#{current_page + 1}"
  end
end

# rubocop:disable Metrics/BlockLength
TwinCam.route("posts") do |r|
  @current_page = 1
  # GET "/admin/posts"
  # GET "/admin/posts/"
  r.root do
    @result = Actions::Posts::Index.new(
      Features.pagination_step,
      @current_page
    ).call
    view "admin/posts/index"
  end

  # GET "/admin/posts/new"
  r.is "new" do
    @post = Actions::Posts::New.new(@user_id).call
    view "admin/posts/new"
  end

  r.on Integer, Integer, String do |year, month, slug|
    routing_params = {year: year, month: month, slug: slug}
    @post = Actions::Posts::Show.new(routing_params).call

    # POST "/admin/posts/2018/04/hello-world/update"
    r.post "update" do
      check_csrf!

      updated_post = Actions::Posts::Update
        .new(@post, r.params)
        .call

      if updated_post.errors.empty?
        r.redirect admin_show_post_path(@post)
      else
        unprocessable_entity(@post.errors[:post])
      end
    end

    # GET "/admin/posts/2018/04/hello-world/edit"
    r.get "edit" do
      if @post
        view "admin/posts/edit"
      else
        not_found
      end
    end

    # POST "/admin/posts/2018/04/hello-world/delete"
    r.get "delete" do
      # no-op
    end

    # GET "/admin/posts/2018/04/hello-world"
    r.is do
      if @post
        view "admin/posts/show"
      else
        not_found
      end
    end
  end

  # POST "/admin/posts"
  r.post do
    check_csrf!

    @post = Actions::Posts::Create.new(r.params).call

    if @post.errors.empty?
      flash["notice"] = "Your post is created."
      r.redirect admin_show_post_path(@post)
    else
      unprocessable_entity(@post.errors[:post])
    end
  end

  # GET "/admin/posts/page/3"
  r.on "page" do
    r.on Integer do |page|
      @current_page = page
      @result = Actions::Posts::Index.new(
        Features.pagination_step,
        @current_page
      ).call
      if @result.records.empty?
        not_found
      else
        view "admin/posts/index"
      end
    end
  end
end
# rubocop:enable Metrics/BlockLength
