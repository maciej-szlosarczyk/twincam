# frozen_string_literal: true

# /admin/ routes
TwinCam.route("admin") do |r|
  rodauth.require_authentication
  @user_id = session["account_id"]

  # GET /admin
  # GET /admin/
  r.root do
    view "admin/index"
  end

  r.on "pages" do
    r.route "pages"
  end

  r.on "posts" do
    r.route "posts"
  end

  r.on "comments" do
    r.route "comments"
  end
end
