# frozen_string_literal: true

# This module grants easy access to error functions.
# route do |r|
#   r.is "foo" do
#     not_found
#   end
# end
module ErrorHandlers
  FORBIDDEN = 403
  NOT_FOUND = 404
  UNPROCESSABLE_ENTITY = 422

  def not_found
    response.status = NOT_FOUND
    view("errors/404")
  end

  def unprocessable_entity(errors = nil)
    response.status = UNPROCESSABLE_ENTITY
    @errors = errors
    view("errors/422")
  end

  def forbidden
    response.status = FORBIDDEN
    view("errors/403")
  end
end
