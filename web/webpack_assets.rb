# frozen_string_literal: true

# This module generates assets tags for ERB
module WebpackAssets
  def self.assets_tag(namespace, type)
    files = json["entrypoints"][namespace][type.to_s]

    files.map { |file_path|
      if type == :css
        css_link(file_path)
      elsif type == :js
        js_link(file_path)
      end
    }.join
  end

  def self.css_link(file_path)
    "<link href='#{file_path}' rel='stylesheet' type='text/css'>"
  end

  def self.js_link(file_path)
    "<script src='#{file_path}'></script>"
  end

  def self.json
    path = File.expand_path("./../public/assets/js/manifest.json", __dir__)
    file = File.read(path)
    JSON.parse(file)
  end
end
