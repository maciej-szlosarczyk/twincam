# frozen_string_literal: true

require "dry-validation"

require File.expand_path("contracts/comment_contract", __dir__)
require File.expand_path("contracts/page_contract", __dir__)
require File.expand_path("contracts/post_contract", __dir__)
require File.expand_path("contracts/user_contract", __dir__)

# Module that holds all contracts between app and its callers
module Contracts
end
