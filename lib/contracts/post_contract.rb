# frozen_string_literal: true

require "dry-validation"
require File.expand_path("../models/post", __dir__)
require File.expand_path("../models/user", __dir__)

module Contracts
  class PostContract < Dry::Validation::Contract
    params do
      required(:post).hash do
        required(:title).filled(type?: String, max_size?: 255)
        required(:body).filled(type?: String)
        required(:author_id).value(:integer)
        optional(:status).value(type?: String, included_in?: ::Post::STATUSES)
      end
    end

    rule(post: :author_id) do
      key.failure("must correspond to a User") unless ::User[value]
    end
  end
end
