# frozen_string_literal: true

require "dry-validation"
require File.expand_path("../models/page", __dir__)
require File.expand_path("../models/user", __dir__)

module Contracts
  class PageContract < Dry::Validation::Contract
    params do
      required(:page).hash do
        required(:title).filled(type?: String, max_size?: 255)
        required(:body).filled(type?: String)
        required(:author_id).value(:integer)
        optional(:status).value(type?: String, included_in?: ::Page::STATUSES)
      end
    end

    rule(page: :author_id) do
      key.failure("must correspond to a User") unless ::User[value]
    end
  end
end
