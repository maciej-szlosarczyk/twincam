# frozen_string_literal: true

require "dry-validation"

require File.expand_path("../models/user", __dir__)

module Contracts
  class UserContract < Dry::Validation::Contract
    schema do
      required(:password).value(type?: String, max_size?: 255, min_size?: 8)
      required(:name).value(type?: String, max_size?: 255, min_size?: 2)
    end
  end

  class UserCreateContract < UserContract
    VALID_EMAIL_REGEX = begin
      /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze
    end

    schema do
      required(:email).value(type?: String, max_size?: 255,
                             format?: VALID_EMAIL_REGEX)
    end

    rule(:email) do
      key.failure("must be unique") if ::User[email: value]
    end
  end
end
