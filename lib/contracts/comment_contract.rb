require "dry-validation"

require File.expand_path("../models/post", __dir__)

module Contracts
  class CommentContract < Dry::Validation::Contract
    params do
      required(:comment).hash do
        required(:body).value(type?: String)
        required(:email).value(type?: String, max_size?: 255)
        required(:name).value(type?: String, max_size?: 255)
        required(:post_id).value(:integer)
      end
    end

    rule(comment: :post_id) do
      key.failure("must correspond to a Post") unless ::Post[value]
    end
  end
end
