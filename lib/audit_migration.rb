# frozen_string_literal: true

# rubocop:disable Metrics/MethodLength

# Builds queries and triggers that are used for auditing tables. Just give it a
# tablename.
class AuditMigration
  attr_reader :table_name

  def initialize(table_name)
    @table_name = table_name
  end

  def create_table
    <<~SQL
      CREATE TABLE IF NOT EXISTS audit.#{table_name} (
           id                 serial NOT NULL,
           object_id          bigint,
           action TEXT NOT NULL CHECK (action IN ('INSERT', 'UPDATE', 'DELETE', 'TRUNCATE')),
           recorded_at        timestamp without time zone,
           old_value          jsonb,
           new_value          jsonb
        );


        ALTER TABLE audit.#{table_name} ADD PRIMARY KEY (id);
        CREATE INDEX ON audit.#{table_name} USING btree (object_id);
        CREATE INDEX ON audit.#{table_name} USING btree (recorded_at)
    SQL
  end

  def create_trigger
    <<~SQL
      CREATE OR REPLACE FUNCTION process_#{table_name}_audit()
      RETURNS TRIGGER AS $process_#{table_name}_audit$
        BEGIN
          IF (TG_OP = 'INSERT') THEN
            INSERT INTO audit.#{table_name}
            (object_id, action, recorded_at, old_value, new_value)
            VALUES (NEW.id, 'INSERT', now(), '{}', to_json(NEW)::jsonb);
            RETURN NEW;
          ELSEIF (TG_OP = 'UPDATE') THEN
            INSERT INTO audit.#{table_name}
            (object_id, action, recorded_at, old_value, new_value)
            VALUES (NEW.id, 'UPDATE', now(), to_json(OLD)::jsonb, to_json(NEW)::jsonb);
            RETURN NEW;
          ELSEIF (TG_OP = 'DELETE') THEN
            INSERT INTO audit.#{table_name}
            (object_id, action, recorded_at, old_value, new_value)
            VALUES (OLD.id, 'DELETE', now(), to_json(OLD)::jsonb, '{}');
            RETURN OLD;
          END IF;
          RETURN NULL;
        END
      $process_#{table_name}_audit$ LANGUAGE plpgsql;

      --- Create the actual trigger
      CREATE TRIGGER process_#{table_name}_audit
      AFTER INSERT OR UPDATE OR DELETE ON #{table_name}
      FOR EACH ROW EXECUTE PROCEDURE process_#{table_name}_audit();
    SQL
  end

  def drop
    <<~SQL
      DROP TRIGGER IF EXISTS process_#{table_name}_audit ON #{table_name};
      DROP FUNCTION IF EXISTS process_#{table_name}_audit();
      DROP TABLE IF EXISTS audit.#{table_name};
    SQL
  end
end
# rubocop:enable Metrics/MethodLength
