# frozen_string_literal: true

require "singleton"
require "logger"

module ApplicationLogger
  # Class for standard out logger, used for migrations and development
  class StandardOut
    include Singleton

    def self.logger
      @logger ||= Logger.new($stdout)
    end
  end

  # Class for File logging, useful for production.
  class File
    include Singleton

    def self.logger
      @logger ||= Logger.new("log/application.log", "daily")
    end
  end
end
