# frozen_string_literal: true

require File.expand_path("actions/comments", __dir__)
require File.expand_path("actions/pages", __dir__)
require File.expand_path("actions/posts", __dir__)
require File.expand_path("actions/users", __dir__)

# Module for actions associated with web endpoints
module Actions
end
