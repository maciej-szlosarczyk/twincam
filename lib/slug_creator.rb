# frozen_string_literal: true

# Class used to create unique slugs
class SlugCreator
  attr_reader :title
  attr_reader :sluggable_class
  attr_reader :result

  def initialize(title, sluggable_class)
    @title = title
    @sluggable_class = sluggable_class
  end

  def find_unique_slug
    @result = title.to_s
    @result = result.gsub(/\W/, " ")
    @result = result.downcase
    @result = result.strip
    @result = result.gsub(/\s/, "-")

    existing_slug_count = similar_items_count
    @result += "-#{existing_slug_count}" if existing_slug_count.positive?
  end

  private

  def similar_items_count
    sluggable_class.where(slug: Regexp.new(@result)).count
  end
end
