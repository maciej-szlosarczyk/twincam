# frozen_string_literal: true

require File.expand_path("../slug_creator", __dir__)

# Main object for a blog, obviously
class Page < Sequel::Model
  STATUSES = %w[draft published].freeze

  plugin :timestamps

  many_to_one :user, key: :author_id

  ##
  # Returns User instance.
  def author
    user
  end

  ##
  # Before validating, creates a unique slug if none was given at the beginning.
  #
  # Returns Post instance.
  def before_validation
    super

    create_unique_slug
  end

  ##
  # Returns String that can be used for constructing URLs,
  # such as "some-title"
  def url_path
    slug
  end

  private

  def create_unique_slug
    slug_creator = SlugCreator.new(title, self.class)
    slug_creator.find_unique_slug
    self.slug ||= slug_creator.result
  end
end
