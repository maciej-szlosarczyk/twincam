# frozen_string_literal: true

require "bcrypt"
require File.expand_path("validators", __dir__)

# User model. Also handles authentication.
class User < Sequel::Model
  include Validators::EmailValidator

  plugin :timestamps
  one_to_many :posts, key: :author_id

  def password=(new_password)
    self.password_digest = BCrypt::Password.create(new_password)
  end
end
