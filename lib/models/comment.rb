# frozen_string_literal: true

require "digest"

require File.expand_path("validators", __dir__)

# Comments, cannot live without posts
class Comment < Sequel::Model
  plugin :timestamps

  # Associations
  # Note: Dependant destroy is handled by Post model
  many_to_one :post, key: :post_id

  def email=(new_email)
    self.email_digest = Digest::SHA256.hexdigest(new_email)
    self[:email] = new_email
  end
end
