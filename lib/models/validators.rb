# frozen_string_literal: true

module Validators
  # Email validator module to include in models
  module EmailValidator
    VALID_EMAIL_REGEX = begin
      /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze
    end

    def validate
      super

      validates_format VALID_EMAIL_REGEX, :email
    end
  end
end
