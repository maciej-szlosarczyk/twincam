# frozen_string_literal: true

require File.expand_path("../slug_creator", __dir__)

# Main object for a blog, obviously
class Post < Sequel::Model
  STATUSES = %w[draft published].freeze

  plugin :timestamps

  many_to_one :user, key: :author_id
  one_to_many :comments, key: :post_id
  plugin :association_dependencies, comments: :destroy

  ##
  # Returns User instance.
  def author
    user
  end

  # Return Numeric count of associated comments
  def comment_count
    comments.count
  end

  ##
  # Before validating, creates a unique slug if none was given at the beginning.
  #
  # Returns Post instance.
  def before_validation
    super

    create_unique_slug
  end

  ##
  # Validate that post has :title and :body that are not blank strings
  def validate
    super

    validate_body
    validate_title
    errors.add(:author_id, "cannot be empty") unless author_id
  end

  ##
  # Returns String that can be used for constructing URLs,
  # such as "/2018/05/some-title"
  def url_path
    return unless slug

    date = created_at.strftime("%Y/%m")

    "#{date}/#{slug}"
  end

  private

  def validate_title
    return unless title

    errors.add(:title, "cannot be empty") if title.empty?
  end

  def validate_body
    return unless body

    errors.add(:body, "cannot be empty") if body.empty?
  end

  def create_unique_slug
    slug_creator = SlugCreator.new(title, self.class)
    slug_creator.find_unique_slug
    self.slug ||= slug_creator.result
  end
end
