# frozen_string_literal: true

begin
  require File.expand_path(".env.rb", __dir__)
rescue LoadError
  raise ".env.rb configuration file not found"
end

require "sequel/core"

Sequel.default_timezone = :utc
Sequel.application_timezone = :local

# Delete TWIN_CAM_DATABASE_URL from the environment, so it isn't accidently
# passed to subprocesses.  TWIN_CAM_DATABASE_URL may contain passwords.
DB = Sequel.connect(
  ENV.delete("TWIN_CAM_DATABASE_URL") || ENV.delete("DATABASE_URL")
)

DB.extension :pg_enum
