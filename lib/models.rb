# frozen_string_literal: true

require File.expand_path("application_logger", __dir__)
require File.expand_path("db", __dir__)
require "sequel/model"

Sequel::Model.cache_associations = false if ENV["RACK_ENV"] == "development"

Sequel::Model.plugin :auto_validations
Sequel::Model.plugin :prepared_statements
Sequel::Model.plugin :subclasses unless ENV["RACK_ENV"] == "development"
Sequel::Model.plugin :association_dependencies

unless defined?(Unreloader)
  require "rack/unreloader"
  Unreloader = Rack::Unreloader.new(reload: false)
end

Unreloader.require("lib/models") do |f|
  Sequel::Model.send(:camelize, File.basename(f).delete_suffix(".rb"))
end

if ENV["RACK_ENV"] == "development"
  DB.loggers << ApplicationLogger::StandardOut.logger
elsif ENV["RACK_ENV"] == "production"
  DB.loggers << ApplicationLogger::StandardOut.logger
  Sequel::Model.freeze_descendents
  DB.freeze
end
