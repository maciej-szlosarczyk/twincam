# frozen_string_literal: true

require File.expand_path("posts/create", __dir__)
require File.expand_path("posts/index", __dir__)
require File.expand_path("posts/new", __dir__)
require File.expand_path("posts/show", __dir__)
require File.expand_path("posts/update", __dir__)

module Actions
  # Actions for Posts
  module Posts
  end
end
