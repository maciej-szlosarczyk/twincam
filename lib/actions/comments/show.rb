# frozen_string_literal: true

require File.expand_path("../../models/comment", __dir__)

module Actions
  module Comments
    # Action for showing the comment
    class Show
      attr_reader :params
      attr_reader :relation

      def initialize(params = {})
        @params = params
        @relation = Comment.eager(:post)
      end

      def call
        comment_id = params[:comment_id]

        find_by_comment_id(comment_id)
      end

      private

      def find_by_comment_id(comment_id)
        relation[id: comment_id]
      end
    end
  end
end
