# frozen_string_literal: true

require File.expand_path("../../contracts/comment_contract", __dir__)

module Actions
  module Comments
    # Action for creating Comment objects.
    class Create
      attr_reader :params
      attr_reader :comment

      def initialize(params = {})
        @params = params
        @comment = Comment.new
      end

      def call
        contract = Contracts::CommentContract.new
        result = contract.call(params)

        if result.success?
          comment_params = result.values[:comment]
          comment.set_fields(comment_params, %i[body email post_id name])
          comment.save
        else
          result
        end
      end
    end
  end
end
