# frozen_string_literal: true

module Actions
  module Comments
    # Delete the supplied comment object.
    class Delete
      attr_reader :comment

      def initialize(comment)
        @comment = comment
      end

      # If a comment is delete, return the Post it was attached to. It is useful
      # for redirecting to the right place in web interface after that.
      def call
        if comment.delete
          comment.post
        else
          false
        end
      end
    end
  end
end
