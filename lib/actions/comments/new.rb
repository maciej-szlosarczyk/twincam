# frozen_string_literal: true

require File.expand_path("../../models/comment", __dir__)

module Actions
  module Comments
    # Used for filling in email data automagically if you are logged in.
    class New
      attr_reader :user_id
      attr_reader :post_id
      attr_reader :comment

      def initialize(post_id, user_id = nil)
        @post_id = post_id
        @user_id = user_id
      end

      def call
        if user_id
          user = User[user_id]
          @comment = Comment.new(post_id: post_id, email: user.email,
                                 name: user.name)
        else
          @comment = Comment.new(post_id: post_id)
        end

        comment
      end
    end
  end
end
