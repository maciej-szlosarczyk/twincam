# frozen_string_literal: true

require File.expand_path("../../models/post", __dir__)

module Actions
  module Posts
    # Used for setting up "/admin/posts/new" view
    class New
      attr_reader :user_id
      attr_reader :post

      def initialize(user_id)
        @user_id = user_id
      end

      def call
        @post = Post.new(author_id: user_id)

        post
      end
    end
  end
end
