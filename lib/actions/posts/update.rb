# frozen_string_literal: true

require File.expand_path("../../contracts/post_contract", __dir__)

module Actions
  module Posts
    # Action for updating Post objects
    class Update
      attr_reader :post
      attr_reader :params

      def initialize(post, params = {})
        @post = post
        @params = params
      end

      def call
        contract = Contracts::PostContract.new
        result = contract.call(params)

        if result.success?
          post_params = result.values[:post]
          post.set_fields(post_params, %i[title body status])
          post.save
        else
          result
        end
      end
    end
  end
end
