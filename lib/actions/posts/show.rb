# frozen_string_literal: true

module Actions
  module Posts
    # Action for showing single Post object
    class Show
      attr_reader :params
      attr_reader :relation

      def initialize(params = {})
        @params = params
        @relation = Post.eager(:user, :comments)
      end

      def call
        month = params[:month]
        year = params[:year]
        slug = params[:slug]
        status = params[:status]

        @relation = find_by_month_year_and_slug(month, year, slug)
        find_by_status_or_return_first(status)
      end

      private

      def find_by_status_or_return_first(status)
        if status
          relation.where(status: status).first
        else
          relation.first
        end
      end

      def find_by_month_year_and_slug(month, year, slug)
        relation.where(Sequel.lit("extract(MONTH from created_at) = ?", month))
          .where(Sequel.lit("extract(YEAR from created_at) = ?", year))
          .where(slug: slug)
      end
    end
  end
end
