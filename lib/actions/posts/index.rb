# frozen_string_literal: true

require File.expand_path("../../models/post", __dir__)

module Actions
  module Posts
    # Action for showing multiple Post object
    class Index
      attr_reader :params
      attr_reader :paginate_by
      attr_reader :page
      attr_reader :result

      def initialize(paginate_by, page, params = {})
        @page = page
        @paginate_by = paginate_by
        @params = params
      end

      def call
        build_result
        query
        fill_result

        result
      end

      private

      def build_result
        @result = OpenStruct.new(page: @page, records: nil, total_pages: nil,
                                 relation: nil)
      end

      def query
        relation = Post.eager(:user)
        relation = select_query(relation)
        relation = limit_query(relation)

        result.relation = relation
      end

      def select_query(relation)
        relation = relation.select_append { count.function.*.over }
        relation.where(params)
      end

      def limit_query(relation)
        relation = relation.order(Sequel.desc(:created_at))
        relation.limit(paginate_by, (page - 1) * paginate_by)
      end

      def fill_result
        result.records = result.relation.all

        if result.records.empty?
          result.total_pages = 1
        else
          count_pages
        end
      end

      def count_pages
        count = result.relation.first.values[:count]

        if count < paginate_by
          result.total_pages = 1
        else
          result.total_pages = count / paginate_by
          result.total_pages += 1
        end
      end
    end
  end
end
