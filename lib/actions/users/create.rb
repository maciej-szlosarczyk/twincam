# frozen_string_literal: true

require File.expand_path("../../contracts/user_contract", __dir__)

module Actions
  module Users
    # Actions for creating new users. Not exposed externally.
    class Create
      attr_reader :user
      attr_reader :params

      def initialize(params = {}, user = User.new)
        @user = user
        @params = params
      end

      def call
        contract = Contracts::UserContract.new
        result = contract.call(params)

        if result.success?
          user.set_fields(params, %i[email name password])
          user.save
        else
          result
        end
      end
    end
  end
end
