# frozen_string_literal: true

require File.expand_path("pages/create", __dir__)
require File.expand_path("pages/index", __dir__)
require File.expand_path("pages/new", __dir__)
require File.expand_path("pages/show", __dir__)
require File.expand_path("pages/update", __dir__)

module Actions
  # Actions for Pages
  module Pages
  end
end
