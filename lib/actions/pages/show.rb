# frozen_string_literal: true

module Actions
  module Pages
    # Action for showing single Page object
    class Show
      attr_reader :params
      attr_reader :relation

      def initialize(params = {})
        @params = params
        @relation = Page.eager(:user)
      end

      def call
        slug = params[:slug]
        status = params[:status]

        if status
          find_by_slug_and_status(slug, status)
        else
          find_by_slug(slug)
        end
      end

      private

      def find_by_slug(slug)
        relation.where(slug: slug).first
      end

      def find_by_slug_and_status(slug, status)
        relation.where(slug: slug, status: status).first
      end
    end
  end
end
