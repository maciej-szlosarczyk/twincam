# frozen_string_literal: true

require File.expand_path("../../models/page", __dir__)

module Actions
  module Pages
    # Used for setting up "/admin/pages/new" view
    class New
      attr_reader :user_id
      attr_reader :page

      def initialize(user_id)
        @user_id = user_id
      end

      def call
        @page = Page.new(author_id: user_id)

        page
      end
    end
  end
end
