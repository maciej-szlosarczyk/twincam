# frozen_string_literal: true

require File.expand_path("../../models/page", __dir__)

module Actions
  module Pages
    # Action for showing multiple Page object
    class Index
      attr_reader :params
      attr_reader :relation

      def initialize(params = {})
        @params = params
      end

      def call
        @relation = Page.eager(:user)
        @relation = relation.where(params)

        relation.order(Sequel.desc(:created_at)).all
      end
    end
  end
end
