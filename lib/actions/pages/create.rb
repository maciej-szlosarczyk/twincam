# frozen_string_literal: true

require File.expand_path("../../contracts/page_contract", __dir__)

module Actions
  module Pages
    # Action for updating Page objects
    class Create
      attr_reader :page
      attr_reader :params

      def initialize(params = {}, page = Page.new)
        @page = page
        @params = params
      end

      def call
        contract = Contracts::PageContract.new
        result = contract.call(params)

        if result.success?
          page_params = result.values[:page]
          page.set_fields(page_params, %i[author_id title body])
          page.save
        else
          result
        end
      end
    end
  end
end
