# frozen_string_literal: true

require File.expand_path("../../contracts/page_contract", __dir__)

module Actions
  module Pages
    # Action for updating Page objects
    class Update
      attr_reader :page
      attr_reader :params

      def initialize(page, params = {})
        @page = page
        @params = params
      end

      def call
        contract = Contracts::PageContract.new
        result = contract.call(params)

        if result.success?
          page_params = result.values[:page]
          page.set_fields(page_params, %i[title body status])
          page.save
        else
          result
        end
      end
    end
  end
end
