# frozen_string_literal: true

require File.expand_path("comments/new", __dir__)
require File.expand_path("comments/create", __dir__)
require File.expand_path("comments/show", __dir__)
require File.expand_path("comments/delete", __dir__)

module Actions
  # Actions for comments
  module Comments
  end
end
