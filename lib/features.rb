# Wrap environment variables in a module
module Features
  def self.pagination_step
    ENV["TWIN_CAM_PAGINATION"].to_i
  end

  def self.spam_timeout
    ENV["TWIN_CAM_SPAM_TIMEOUT"].to_i
  end

  def self.google_site_verification
    ENV["GOOGLE_SITE_VERIFICATION"]
  end
end
