ENV["RACK_ENV"] ||= "development"

ENV["TWIN_CAM_SESSION_SECRET"] ||= "\"GT0vgZ+7okFE9mC/wjyw+nN9KJUMNwXvLjaBZAeqbupqQnsOUncKNXz22oo+\\nNS9DjORVDlgFlIAFwpfRT9lGzQ==\\n\""
ENV["TWIN_CAM_DATABASE_URL"] ||= "postgres://#{ENV["APP_DBHOST"]}/twincam_test?user=twincam"
ENV["ROLLBAR_SECRET_KEY"] ||= "Your rollbar secret token"

ENV["TWIN_CAM_PAGINATION"] ||= "10"
ENV["TWIN_CAM_SPAM_TIMEOUT"] ||= "3"
ENV["GOOGLE_SITE_VERIFICATION"] ||= "some token"
