--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: audit; Type: SCHEMA; Schema: -; Owner: twincam
--

CREATE SCHEMA audit;


ALTER SCHEMA audit OWNER TO twincam;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: page_statuses; Type: TYPE; Schema: public; Owner: twincam
--

CREATE TYPE public.page_statuses AS ENUM (
    'draft',
    'published'
);


ALTER TYPE public.page_statuses OWNER TO twincam;

--
-- Name: post_statuses; Type: TYPE; Schema: public; Owner: twincam
--

CREATE TYPE public.post_statuses AS ENUM (
    'draft',
    'published'
);


ALTER TYPE public.post_statuses OWNER TO twincam;

--
-- Name: process_comments_audit(); Type: FUNCTION; Schema: public; Owner: twincam
--

CREATE FUNCTION public.process_comments_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF (TG_OP = 'INSERT') THEN
      INSERT INTO audit.comments
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'INSERT', now(), '{}', to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
      INSERT INTO audit.comments
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'UPDATE', now(), to_json(OLD)::jsonb, to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
      INSERT INTO audit.comments
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (OLD.id, 'DELETE', now(), to_json(OLD)::jsonb, '{}');
      RETURN OLD;
    END IF;
    RETURN NULL;
  END
$$;


ALTER FUNCTION public.process_comments_audit() OWNER TO twincam;

--
-- Name: process_pages_audit(); Type: FUNCTION; Schema: public; Owner: twincam
--

CREATE FUNCTION public.process_pages_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF (TG_OP = 'INSERT') THEN
      INSERT INTO audit.pages
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'INSERT', now(), '{}', to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
      INSERT INTO audit.pages
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'UPDATE', now(), to_json(OLD)::jsonb, to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
      INSERT INTO audit.pages
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (OLD.id, 'DELETE', now(), to_json(OLD)::jsonb, '{}');
      RETURN OLD;
    END IF;
    RETURN NULL;
  END
$$;


ALTER FUNCTION public.process_pages_audit() OWNER TO twincam;

--
-- Name: process_posts_audit(); Type: FUNCTION; Schema: public; Owner: twincam
--

CREATE FUNCTION public.process_posts_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF (TG_OP = 'INSERT') THEN
      INSERT INTO audit.posts
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'INSERT', now(), '{}', to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
      INSERT INTO audit.posts
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'UPDATE', now(), to_json(OLD)::jsonb, to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
      INSERT INTO audit.posts
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (OLD.id, 'DELETE', now(), to_json(OLD)::jsonb, '{}');
      RETURN OLD;
    END IF;
    RETURN NULL;
  END
$$;


ALTER FUNCTION public.process_posts_audit() OWNER TO twincam;

--
-- Name: process_users_audit(); Type: FUNCTION; Schema: public; Owner: twincam
--

CREATE FUNCTION public.process_users_audit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
    IF (TG_OP = 'INSERT') THEN
      INSERT INTO audit.users
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'INSERT', now(), '{}', to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
      INSERT INTO audit.users
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (NEW.id, 'UPDATE', now(), to_json(OLD)::jsonb, to_json(NEW)::jsonb);
      RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
      INSERT INTO audit.users
      (object_id, action, recorded_at, old_value, new_value)
      VALUES (OLD.id, 'DELETE', now(), to_json(OLD)::jsonb, '{}');
      RETURN OLD;
    END IF;
    RETURN NULL;
  END
$$;


ALTER FUNCTION public.process_users_audit() OWNER TO twincam;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: comments; Type: TABLE; Schema: audit; Owner: twincam
--

CREATE TABLE audit.comments (
    id integer NOT NULL,
    object_id bigint,
    action text NOT NULL,
    recorded_at timestamp without time zone,
    old_value jsonb,
    new_value jsonb,
    CONSTRAINT comments_action_check CHECK ((action = ANY (ARRAY['INSERT'::text, 'UPDATE'::text, 'DELETE'::text, 'TRUNCATE'::text])))
);


ALTER TABLE audit.comments OWNER TO twincam;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: audit; Owner: twincam
--

CREATE SEQUENCE audit.comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.comments_id_seq OWNER TO twincam;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: twincam
--

ALTER SEQUENCE audit.comments_id_seq OWNED BY audit.comments.id;


--
-- Name: pages; Type: TABLE; Schema: audit; Owner: twincam
--

CREATE TABLE audit.pages (
    id integer NOT NULL,
    object_id bigint,
    action text NOT NULL,
    recorded_at timestamp without time zone,
    old_value jsonb,
    new_value jsonb,
    CONSTRAINT pages_action_check CHECK ((action = ANY (ARRAY['INSERT'::text, 'UPDATE'::text, 'DELETE'::text, 'TRUNCATE'::text])))
);


ALTER TABLE audit.pages OWNER TO twincam;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: audit; Owner: twincam
--

CREATE SEQUENCE audit.pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.pages_id_seq OWNER TO twincam;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: twincam
--

ALTER SEQUENCE audit.pages_id_seq OWNED BY audit.pages.id;


--
-- Name: posts; Type: TABLE; Schema: audit; Owner: twincam
--

CREATE TABLE audit.posts (
    id integer NOT NULL,
    object_id bigint,
    action text NOT NULL,
    recorded_at timestamp without time zone,
    old_value jsonb,
    new_value jsonb,
    CONSTRAINT posts_action_check CHECK ((action = ANY (ARRAY['INSERT'::text, 'UPDATE'::text, 'DELETE'::text, 'TRUNCATE'::text])))
);


ALTER TABLE audit.posts OWNER TO twincam;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: audit; Owner: twincam
--

CREATE SEQUENCE audit.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.posts_id_seq OWNER TO twincam;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: twincam
--

ALTER SEQUENCE audit.posts_id_seq OWNED BY audit.posts.id;


--
-- Name: users; Type: TABLE; Schema: audit; Owner: twincam
--

CREATE TABLE audit.users (
    id integer NOT NULL,
    object_id bigint,
    action text NOT NULL,
    recorded_at timestamp without time zone,
    old_value jsonb,
    new_value jsonb,
    CONSTRAINT users_action_check CHECK ((action = ANY (ARRAY['INSERT'::text, 'UPDATE'::text, 'DELETE'::text, 'TRUNCATE'::text])))
);


ALTER TABLE audit.users OWNER TO twincam;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: audit; Owner: twincam
--

CREATE SEQUENCE audit.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE audit.users_id_seq OWNER TO twincam;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: audit; Owner: twincam
--

ALTER SEQUENCE audit.users_id_seq OWNED BY audit.users.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: twincam
--

CREATE TABLE public.comments (
    id integer NOT NULL,
    post_id integer NOT NULL,
    body text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    name character varying(255) NOT NULL,
    uuid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    email_digest character varying(255) NOT NULL,
    email character varying(255)
);


ALTER TABLE public.comments OWNER TO twincam;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: twincam
--

ALTER TABLE public.comments ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: pages; Type: TABLE; Schema: public; Owner: twincam
--

CREATE TABLE public.pages (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    body text,
    author_id integer NOT NULL,
    status public.page_statuses DEFAULT 'draft'::public.page_statuses NOT NULL,
    uuid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE public.pages OWNER TO twincam;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: twincam
--

ALTER TABLE public.pages ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: posts; Type: TABLE; Schema: public; Owner: twincam
--

CREATE TABLE public.posts (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    slug character varying(255) NOT NULL,
    body text,
    author_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    uuid uuid DEFAULT public.gen_random_uuid() NOT NULL,
    status public.post_statuses DEFAULT 'draft'::public.post_statuses NOT NULL
);


ALTER TABLE public.posts OWNER TO twincam;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: twincam
--

ALTER TABLE public.posts ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: schema_info; Type: TABLE; Schema: public; Owner: twincam
--

CREATE TABLE public.schema_info (
    version integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.schema_info OWNER TO twincam;

--
-- Name: users; Type: TABLE; Schema: public; Owner: twincam
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying(255) NOT NULL,
    password_digest character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    name character varying(255) NOT NULL,
    uuid uuid DEFAULT public.gen_random_uuid() NOT NULL
);


ALTER TABLE public.users OWNER TO twincam;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: twincam
--

ALTER TABLE public.users ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: comments id; Type: DEFAULT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.comments ALTER COLUMN id SET DEFAULT nextval('audit.comments_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.pages ALTER COLUMN id SET DEFAULT nextval('audit.pages_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.posts ALTER COLUMN id SET DEFAULT nextval('audit.posts_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.users ALTER COLUMN id SET DEFAULT nextval('audit.users_id_seq'::regclass);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: audit; Owner: twincam
--

ALTER TABLE ONLY audit.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: comments comments_uuid_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_uuid_key UNIQUE (uuid);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: pages pages_slug_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_slug_key UNIQUE (slug);


--
-- Name: pages pages_uuid_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_uuid_key UNIQUE (uuid);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: posts posts_slug_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_slug_key UNIQUE (slug);


--
-- Name: posts posts_uuid_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_uuid_key UNIQUE (uuid);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_uuid_key; Type: CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_uuid_key UNIQUE (uuid);


--
-- Name: comments_object_id_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX comments_object_id_idx ON audit.comments USING btree (object_id);


--
-- Name: comments_recorded_at_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX comments_recorded_at_idx ON audit.comments USING btree (recorded_at);


--
-- Name: pages_object_id_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX pages_object_id_idx ON audit.pages USING btree (object_id);


--
-- Name: pages_recorded_at_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX pages_recorded_at_idx ON audit.pages USING btree (recorded_at);


--
-- Name: posts_object_id_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX posts_object_id_idx ON audit.posts USING btree (object_id);


--
-- Name: posts_recorded_at_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX posts_recorded_at_idx ON audit.posts USING btree (recorded_at);


--
-- Name: users_object_id_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX users_object_id_idx ON audit.users USING btree (object_id);


--
-- Name: users_recorded_at_idx; Type: INDEX; Schema: audit; Owner: twincam
--

CREATE INDEX users_recorded_at_idx ON audit.users USING btree (recorded_at);


--
-- Name: pages_author_id_index; Type: INDEX; Schema: public; Owner: twincam
--

CREATE INDEX pages_author_id_index ON public.pages USING btree (author_id);


--
-- Name: posts_author_id_index; Type: INDEX; Schema: public; Owner: twincam
--

CREATE INDEX posts_author_id_index ON public.posts USING btree (author_id);


--
-- Name: posts_created_at_index; Type: INDEX; Schema: public; Owner: twincam
--

CREATE INDEX posts_created_at_index ON public.posts USING btree (created_at);


--
-- Name: posts_slug_index; Type: INDEX; Schema: public; Owner: twincam
--

CREATE UNIQUE INDEX posts_slug_index ON public.posts USING btree (slug);


--
-- Name: comments process_comments_audit; Type: TRIGGER; Schema: public; Owner: twincam
--

CREATE TRIGGER process_comments_audit AFTER INSERT OR DELETE OR UPDATE ON public.comments FOR EACH ROW EXECUTE PROCEDURE public.process_comments_audit();


--
-- Name: pages process_pages_audit; Type: TRIGGER; Schema: public; Owner: twincam
--

CREATE TRIGGER process_pages_audit AFTER INSERT OR DELETE OR UPDATE ON public.pages FOR EACH ROW EXECUTE PROCEDURE public.process_pages_audit();


--
-- Name: posts process_posts_audit; Type: TRIGGER; Schema: public; Owner: twincam
--

CREATE TRIGGER process_posts_audit AFTER INSERT OR DELETE OR UPDATE ON public.posts FOR EACH ROW EXECUTE PROCEDURE public.process_posts_audit();


--
-- Name: users process_users_audit; Type: TRIGGER; Schema: public; Owner: twincam
--

CREATE TRIGGER process_users_audit AFTER INSERT OR DELETE OR UPDATE ON public.users FOR EACH ROW EXECUTE PROCEDURE public.process_users_audit();


--
-- Name: comments comments_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.posts(id);


--
-- Name: pages pages_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- Name: posts posts_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: twincam
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_author_id_fkey FOREIGN KEY (author_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

