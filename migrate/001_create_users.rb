# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:users) do
      primary_key :id
      column :email, String, null: false, size: 255, unique: true
      column :password_digest, String, null: false, size: 255

      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end
  end
end
