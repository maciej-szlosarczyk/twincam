# frozen_string_literal: true

Sequel.migration do
  up do
    run "CREATE EXTENSION pgcrypto;"

    alter_table(:users) do
      add_column :uuid, :uuid, null: false,
                               default: Sequel.function(:gen_random_uuid),
                               unique: true
    end

    alter_table(:posts) do
      add_column :uuid, :uuid, null: false,
                               default: Sequel.function(:gen_random_uuid),
                               unique: true
    end

    alter_table(:comments) do
      add_column :uuid, :uuid, null: false,
                               default: Sequel.function(:gen_random_uuid),
                               unique: true
    end
  end

  down do
    drop_column :posts, :uuid
    drop_column :users, :uuid
    drop_column :comments, :uuid
    run "DROP EXTENSION IF EXISTS pgcrypto"
  end
end
