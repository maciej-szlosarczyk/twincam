# frozen_string_literal: true

Sequel.migration do
  up do
    create_enum(:page_statuses, %w[draft published])

    create_table(:pages) do
      primary_key :id
      column :title, String, size: 255, null: false
      column :slug, String, size: 255, null: false, unique: true
      column :body, String, text: true
      foreign_key :author_id, :users, null: false
      index :author_id

      column :status, :page_statuses, default: "draft", null: false

      column :uuid, :uuid, null: false,
                           default: Sequel.function(:gen_random_uuid),
                           unique: true

      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end
  end

  down do
    drop_table(:pages)
    drop_enum(:page_statuses)
  end
end
