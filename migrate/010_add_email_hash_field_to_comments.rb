# frozen_string_literal: true

Sequel.migration do
  up do
    alter_table(:comments) do
      add_column :email_digest, String, null: false, size: 255
      drop_column :email
    end
  end

  down do
    alter_table(:comments) do
      drop_column :email_digest
      add_column :email, String, null: false, size: 255
    end
  end
end
