# frozen_string_literal: true

require File.expand_path("../lib/audit_migration", __dir__)

Sequel.migration do
  up do
    %w[users posts comments pages].each do |item|
      migration = AuditMigration.new(item)
      run(migration.create_table)
      run(migration.create_trigger)
    end
  end

  down do
    %w[users posts comments pages].each do |item|
      migration = AuditMigration.new(item)
      run(migration.drop)
    end
  end
end
