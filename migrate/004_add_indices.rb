# frozen_string_literal: true

Sequel.migration do
  change do
    alter_table(:posts) do
      add_index :slug, unique: true
      add_index :created_at
    end
  end
end
