# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:posts) do
      primary_key :id
      column :title, String, size: 255, null: false
      column :slug, String, size: 255, null: false, unique: true
      column :body, String, text: true
      foreign_key :author_id, :users, null: false
      index :author_id

      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end
  end
end
