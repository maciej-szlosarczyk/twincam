# frozen_string_literal: true

Sequel.migration do
  up do
    create_enum(:post_statuses, %w[draft published])
    alter_table(:posts) do
      add_column :status, :post_statuses, default: "draft", null: false
    end
  end

  down do
    alter_table(:posts) do
      drop_column :status
    end
    drop_enum :post_statuses
  end
end
