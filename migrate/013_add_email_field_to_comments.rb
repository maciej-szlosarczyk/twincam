# frozen_string_literal: true

Sequel.migration do
  up do
    alter_table(:comments) do
      add_column :email, String, null: true, size: 255
    end
  end

  down do
    alter_table(:comments) do
      drop_column :email
    end
  end
end
