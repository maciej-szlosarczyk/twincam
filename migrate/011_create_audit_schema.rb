# frozen_string_literal: true

Sequel.migration do
  up do
    sql = "CREATE SCHEMA IF NOT EXISTS audit;"
    run(sql)
  end

  down do
    sql = "DROP SCHEMA IF EXISTS audit;"
    run(sql)
  end
end
