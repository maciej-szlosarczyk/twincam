# frozen_string_literal: true

Sequel.migration do
  change do
    alter_table(:comments) do
      add_column :name, String, null: false, size: 255
    end
  end
end
