# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:comments) do
      primary_key :id
      foreign_key :post_id, :posts, null: false
      column :email, String, null: false, size: 255
      column :body, String, null: false, text: true

      column :created_at, DateTime, null: false
      column :updated_at, DateTime
    end
  end
end
