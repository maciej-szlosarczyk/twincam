# frozen_string_literal: true

Sequel.migration do
  change do
    alter_table(:users) do
      add_column :name, String, null: false, size: 255
    end
  end
end
