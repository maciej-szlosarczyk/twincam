# frozen_string_literal: true

dev = ENV["RACK_ENV"] == "development"
prod = ENV["RACK_ENV"] == "production"

require "rollbar"
require File.expand_path("lib/application_logger", __dir__)

Rollbar.configure do |config|
  config.access_token = ENV["ROLLBAR_SECRET_KEY"]
  config.environment = ENV["RACK_ENV"]
end

require "rollbar/middleware/rack"
use Rollbar::Middleware::Rack

if dev
  logger = ApplicationLogger::StandardOut.logger
elsif prod
  logger = ApplicationLogger::StandardOut.logger
end

require "rack/unreloader"
Unreloader = Rack::Unreloader.new(
  subclasses: %w[Roda Sequel::Model], logger: logger, reload: dev
) { TwinCam }

require File.expand_path("lib/models", __dir__)
Unreloader.require("twin_cam.rb") { "TwinCam" }
run(dev ? Unreloader : TwinCam.freeze.app)
