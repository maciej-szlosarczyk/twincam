# frozen_string_literal: true

require File.expand_path("lib/app_container", __dir__)
require File.expand_path("web/route_handlers/error_handlers", __dir__)
require File.expand_path("web/webpack_assets", __dir__)

require "roda"
require "asciidoctor"

# The TwinCam blogging application
class TwinCam < Roda
  include ErrorHandlers
  include WebpackAssets

  plugin :default_headers,
    "Content-Type" => "text/html",
    "Content-Security-Policy" =>
    "default-src 'self'; " \
    "style-src 'self' 'unsafe-inline'; " \
    "script-src 'self' " \
    "",
    "Strict-Transport-Security" => "max-age=16070400;",
    # Uncomment if only allowing https:// access
    "X-Frame-Options" => "deny",
    "X-Content-Type-Options" => "nosniff",
    "X-XSS-Protection" => "1; mode=block"

  plugin :sessions,
    key: "_TwinCam.session",
    # Don't delete session secret from environment in development mode as
    # it breaks reloading
    secret: ENV.send((ENV["RACK_ENV"] == "development" ? :[] : :delete),
      "TWIN_CAM_SESSION_SECRET")
  plugin :flash
  plugin :rodauth, csrf: :route_csrf do
    enable :login, :logout, :change_password
    accounts_table :users
    account_password_hash_column :password_digest
  end

  plugin :static, ["/assets"], root: "public"
  plugin :public

  # Render ERB files
  plugin :render, escape: true,
                  views: File.expand_path("web/views", __dir__)
  # Support for routes in multiple files
  plugin :multi_route
  # Create easy to use resource_path functions
  plugin :path
  # Return 403 on CSRF
  csrf_action = begin
    if ENV["RACK_ENV"] == "development"
      :raise
    else
      :empty_403
    end
  end

  plugin :route_csrf, csrf_failure: csrf_action
  # Handle both GET /blog/ and GET /blog
  plugin :empty_root

  Unreloader.require("web/routes") {}

  route do |r|
    r.multi_route
    r.rodauth

    r.root do
      r.route "blog"
    end

    r.on "public" do
      r.public
    end

    r.on "admin" do
      r.route "admin"
    end

    r.on String do |slug|
      routing_params = {slug: slug, status: "published"}

      @page = Actions::Pages::Show.new(routing_params).call

      if @page
        @page_title = @page.title
        view "pages/show"
      else
        not_found
      end
    end
  end
end
