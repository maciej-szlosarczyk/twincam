import 'typeface-source-sans-pro'
import 'typeface-inconsolata'

document.addEventListener('DOMContentLoaded', () => {
  const notifScope = document.querySelectorAll('.notification .delete') || []

  notifScope.forEach(deleteMe => {
    const notification = deleteMe.parentNode
    deleteMe.addEventListener('click', () => {
      notification.parentNode.removeChild(notification)
    })
  })
})
