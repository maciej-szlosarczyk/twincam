import HighlightJS from 'highlight.js'

document.addEventListener('DOMContentLoaded', () => {
  HighlightJS.initHighlighting()
})
