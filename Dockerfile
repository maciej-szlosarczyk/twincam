FROM registry.gitlab.com/maciej-szlosarczyk/asdf_docker/deploy:latest

USER root
RUN mkdir -p /opt/webapps/app
WORKDIR /opt/webapps/app
RUN chown asdf:asdf -R /opt/webapps/app

USER asdf
COPY .tool-versions ./
RUN asdf plugin-add nodejs
RUN bash "$HOME"/.asdf/plugins/nodejs/bin/import-release-team-keyring
RUN asdf plugin-add ruby
RUN asdf install

COPY Gemfile Gemfile.lock ./
RUN bundle install --jobs 20 --retry 5 --without ci test

RUN npm install -g webpack webpack-cli
COPY webpack.config.js package.json package-lock.json ./
RUN npm install

COPY . ./
RUN webpack --mode production
