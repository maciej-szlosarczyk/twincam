# frozen_string_literal: true

ENV["RACK_ENV"] = "test"

require File.expand_path("../minitest_helper", __dir__)
require File.expand_path("../../lib/models", __dir__)
require File.expand_path("../../lib/contracts", __dir__)

# Base class for contracts tests:
# MyContractTest < TwinCamActionTest
# Ran in parallel by default.
class TwinCamContractTest < TwinCamTest
  def around
    DB.transaction(rollback: :always, savepoint: true, auto_savepoint: true) do
      super
    end
  end

  def around_all
    DB.transaction(rollback: :always) do
      super
    end
  end

  # MiniTest has this awesome ability of running multiple tests at once.
  parallelize_me!
end
