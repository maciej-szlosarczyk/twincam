# frozen_string_literal: true

require File.expand_path("contract_test_helper", __dir__)

class PageContractTest < TwinCamContractTest
  def setup
    super

    @user = Fabricate(:administrator)
    @create_contract = Contracts::PageContract.new
    @default_params = {
      "page" => {
        "title" => "Title", "body" => "Some string",
        "author_id" => @user.id.to_s
      }
    }
  end

  def check_validity(hash)
    @create_contract.call(hash).success?
  end

  def get_errors_hash(hash)
    @create_contract.call(hash).errors.to_h
  end

  # Title
  def test_title_must_be_a_string
    params = @default_params
    params["page"]["title"] = 123
    refute(check_validity(params))
    assert_equal({page: {title: ["must be String"]}}, get_errors_hash(params))

    str = "0" * 254
    params["page"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_title_cannot_be_empty
    params = @default_params
    params["page"]["title"] = ""
    refute(check_validity(params))
    assert_equal({page: {title: ["must be filled"]}}, get_errors_hash(params))

    str = "0" * 254
    params["page"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_title_cannot_be_longer_than_255_chars
    str = "0" * 999
    params = @default_params
    params["page"]["title"] = str

    refute(check_validity(params))
    assert_equal({page: {title: ["size cannot be greater than 255"]}},
      get_errors_hash(params))

    str = "0" * 254
    params["page"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Body
  def test_body_must_be_a_string
    params = @default_params
    params["page"]["body"] = 123

    refute(check_validity(params))
    assert_equal({page: {body: ["must be String"]}}, get_errors_hash(params))

    str = "0" * 254
    params["page"]["body"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Author id
  def test_author_id_must_be_an_integer
    params = @default_params
    params["page"]["author_id"] = "str"
    refute(check_validity(params))
    assert_equal({page: {author_id: ["must be an integer"]}},
      get_errors_hash(params))

    params["page"]["author_id"] = @user.id.to_s
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_author_id_must_correspond_with_a_user
    params = @default_params
    params["page"]["author_id"] = 0
    refute(check_validity(params))
    assert_equal({page: {author_id: ["must correspond to a User"]}},
      get_errors_hash(params))

    params["page"]["author_id"] = @user.id.to_s
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Status
  def test_status_is_optional
    assert(check_validity(@default_params))
  end

  def test_status_can_be_draft_or_published
    params = @default_params
    params["page"]["status"] = "draft"
    assert(check_validity(params))

    params["page"]["status"] = "published"
    assert(check_validity(params))

    params["page"]["status"] = "unknown"
    refute(check_validity(params))
  end
end
