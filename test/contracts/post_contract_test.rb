# frozen_string_literal: true

require File.expand_path("contract_test_helper", __dir__)

class PostContractTest < TwinCamContractTest
  def setup
    super

    @user = Fabricate(:administrator)
    @create_contract = Contracts::PostContract.new
    @default_params = {
      "post" => {
        "title" => "Title", "body" => "Some string", "author_id" => @user.id.to_s
      }
    }
  end

  def check_validity(hash)
    @create_contract.call(hash).success?
  end

  def get_errors_hash(hash)
    @create_contract.call(hash).errors.to_h
  end

  # Title
  def test_title_must_be_a_string
    params = @default_params
    params["post"]["title"] = 123
    refute(check_validity(params))
    assert_equal({post: {title: ["must be String"]}}, get_errors_hash(params))

    str = "0" * 254
    params["post"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_title_cannot_be_empty
    params = @default_params
    params["post"]["title"] = ""
    refute(check_validity(params))
    assert_equal({post: {title: ["must be filled"]}}, get_errors_hash(params))

    str = "0" * 254
    params["post"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_title_cannot_be_longer_than_255_chars
    str = "0" * 999
    params = @default_params
    params["post"]["title"] = str

    refute(check_validity(params))
    assert_equal({post: {title: ["size cannot be greater than 255"]}},
      get_errors_hash(params))

    str = "0" * 254
    params["post"]["title"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Body
  def test_body_must_be_a_string
    params = @default_params
    params["post"]["body"] = 123
    refute(check_validity(params))
    assert_equal({post: {body: ["must be String"]}}, get_errors_hash(params))

    str = "0" * 254
    params["post"]["body"] = str
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Author id
  def test_author_id_must_be_an_integer
    params = @default_params
    params["post"]["author_id"] = "foo"
    refute(check_validity(params))
    assert_equal({post: {author_id: ["must be an integer"]}}, get_errors_hash(params))

    params["post"]["author_id"] = @user.id
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_author_id_must_correspond_with_a_user
    params = @default_params
    params["post"]["author_id"] = 0
    refute(check_validity(params))
    assert_equal({post: {author_id: ["must correspond to a User"]}},
      get_errors_hash(params))

    params["post"]["author_id"] = @user.id
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Status
  def test_status_is_optional
    assert(check_validity(@default_params))
  end

  def test_status_can_be_draft_or_published
    params = @default_params
    params["post"]["status"] = "draft"
    assert(check_validity(params))

    params["post"]["status"] = "published"
    assert(check_validity(params))

    params["post"]["status"] = "unknown"
    refute(check_validity(params))
  end
end
