# frozen_string_literal: true

require File.expand_path("contract_test_helper", __dir__)

class CommentContractTest < TwinCamContractTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @create_contract = Contracts::CommentContract.new
  end

  def check_validity(hash)
    @create_contract.call(hash).success?
  end

  def get_errors_hash(hash)
    @create_contract.call(hash).errors.to_h
  end

  # Email
  def test_email_must_be_a_string
    params = {"comment" => {"email" => 123, "body" => "123",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {email: ["must be String"]}},
      get_errors_hash(params))
  end

  def test_email_cannot_be_nil
    params = {"comment" => {"email" => nil, "body" => "123",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {email: ["must be String"]}},
      get_errors_hash(params))
  end

  def test_email_cannot_be_longer_than_255_chars
    str = "0" * 999 + "email@test.com"
    params = {"comment" => {"email" => str, "body" => "123",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {email: ["size cannot be greater than 255"]}},
      get_errors_hash(params))

    str = "email@example.com"
    params = {"comment" => {"email" => str, "body" => "123",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Body
  def test_body_must_be_a_string
    params = {"comment" => {"email" => "email@example.com", "body" => 123,
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {body: ["must be String"]}}, get_errors_hash(params))

    str = "0" * 254
    params = {"comment" => {"email" => "email@example.com", "body" => str,
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Post id
  def test_post_id_must_be_an_integer
    params = {"comment" => {"email" => "email@example.com", "body" => "Body",
                            "post_id" => "abc",
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {post_id: ["must be an integer"]}}, get_errors_hash(params))

    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_post_id_must_correspond_with_a_post
    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => 0,
                            "name" => "foo"}}
    refute(check_validity(params))
    assert_equal({comment: {post_id: ["must correspond to a Post"]}},
      get_errors_hash(params))

    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => "foo"}}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  # Name
  def test_name_must_be_a_string
    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => 123}}
    refute(check_validity(params))
    assert_equal({comment: {name: ["must be String"]}},
      get_errors_hash(params))
  end

  def test_name_cannot_be_nil
    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => nil}}
    refute(check_validity(params))
    assert_equal({comment: {name: ["must be String"]}},
      get_errors_hash(params))
  end

  def test_name_cannot_be_longer_than_255_chars
    str = "0" * 999
    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => str}}
    refute(check_validity(params))
    assert_equal({comment: {name: ["size cannot be greater than 255"]}},
      get_errors_hash(params))

    str = "Johnny Appleseed"
    params = {"comment" => {"email" => "email@example.com", "body" => "abc",
                            "post_id" => @post.id.to_s,
                            "name" => str}}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end
end
