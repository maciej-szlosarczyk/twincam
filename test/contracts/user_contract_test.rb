# frozen_string_literal: true

require File.expand_path("contract_test_helper", __dir__)

class UserContractTest < TwinCamContractTest
  def setup
    super

    Fabricate(:administrator)
    @create_contract = Contracts::UserCreateContract.new
  end

  def check_validity(hash)
    @create_contract.call(hash).success?
  end

  def get_errors_hash(hash)
    @create_contract.call(hash).errors.to_h
  end

  # Password
  def test_password_must_be_a_string
    params = {password: 123, email: "123@account.com", name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({password: ["must be String"]},
      get_errors_hash(params))
  end

  def test_password_must_be_longer_than_8_chars
    params = {password: "123", email: "123@account.com", name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({password: ["size cannot be less than 8"]},
      get_errors_hash(params))
  end

  def test_password_cannot_be_nil
    params = {password: nil, email: "123@account.com", name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({password: ["must be String"]},
      get_errors_hash(params))
  end

  # Email
  def test_email_must_be_a_string
    params = {password: "password", email: 123, name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({email: ["must be String"]}, get_errors_hash(params))
  end

  def test_email_cannot_be_nil
    params = {password: "password", email: nil, name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({email: ["must be String"]}, get_errors_hash(params))
  end

  def test_email_cannot_be_longer_than_255_chars
    str = "0" * 999
    params = {password: "password", email: str, name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({email: ["size cannot be greater than 255"]},
      get_errors_hash(params))

    str = "email@example.com"
    params = {password: "password", email: str, name: "Test Admin"}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end

  def test_email_must_be_unique
    str = "test@email.com"
    params = {password: "password", email: str, name: "Test Admin"}
    refute(check_validity(params))
    assert_equal({email: ["must be unique"]}, get_errors_hash(params))
  end

  # Name
  def test_name_must_be_a_string
    params = {password: "password", name: 123, email: "unique@email.com"}
    refute(check_validity(params))
    assert_equal({name: ["must be String"]}, get_errors_hash(params))
  end

  def test_name_cannot_be_nil
    params = {password: "password", name: nil, email: "unique@email.com"}
    refute(check_validity(params))
    assert_equal({name: ["must be String"]}, get_errors_hash(params))
  end

  def test_name_cannot_be_longer_than_255_chars
    str = "0" * 999
    params = {password: "password", name: str, email: "unique@email.com"}
    refute(check_validity(params))
    assert_equal({name: ["size cannot be greater than 255"]},
      get_errors_hash(params))

    str = "Test Admin"
    params = {password: "password", name: str, email: "unique@email.com"}
    assert(check_validity(params))
    assert_equal({}, get_errors_hash(params))
  end
end
