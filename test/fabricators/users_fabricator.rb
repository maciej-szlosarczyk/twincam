# frozen_string_literal: true

Fabricator(:administrator, from: User) do
  email "test@email.com"
  name "Test Admin"
  password "password"
  password_digest BCrypt::Password.create("password")
end
