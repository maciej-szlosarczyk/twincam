# frozen_string_literal: true

Fabricator(:page, from: Page) do
end

Fabricator(:privacy_policy_page, from: :page) do
  title "privacy policy"
  slug "privacy-policy"
  body "https://example.com[privacy policy!]"
  created_at Time.parse("2018-04-01 11:00 +0000")
  updated_at Time.parse("2018-04-01 11:00 +0000")
  status "published"
end

Fabricator(:second_page, from: :page) do
  title "Second page"
  slug "second-page"
  body "This is just another page on this website. Move along."
  created_at Time.parse("2018-04-10 11:00 +0000")
  updated_at Time.parse("2018-04-10 11:00 +0000")
  status "published"
end

Fabricator(:draft_page, from: :page) do
  title "Draft Page"
  slug "draft-page"
  body "This text is not visible"
  created_at Time.parse("2018-04-15 11:00 +0000")
  updated_at Time.parse("2018-04-15 11:00 +0000")
  status "draft"
end
