# frozen_string_literal: true

Fabricator(:first_comment, from: Comment) do
  email_digest Digest::SHA256.hexdigest("user@email.com")
  name "Johnny Commenter"
  body "Some comment body"
  created_at Date.parse("2018-04-01 11:00 +0000")
  updated_at Date.parse("2018-04-01 11:00 +0000")
end
