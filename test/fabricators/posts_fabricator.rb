# frozen_string_literal: true

Fabricator(:post, from: Post) do
end

Fabricator(:hello_world_post, from: :post) do
  title "hello world!"
  slug "hello-world"
  body "https://example.com[hello world!]"
  created_at Time.parse("2018-04-01 11:00 +0000")
  updated_at Time.parse("2018-04-01 11:00 +0000")
  status "published"
end

Fabricator(:second_post, from: :post) do
  title "Second post!"
  slug "second-post"
  body <<~TEXT.strip
    .app.rb
    [source,ruby]
    ----
    require 'sinatra'

    get '/hi' do
    "Hello World!"
    end
    ----
  TEXT
  created_at Time.parse("2018-04-10 11:00 +0000")
  updated_at Time.parse("2018-04-10 11:00 +0000")
  status "published"
end

Fabricator(:draft_post, from: :post) do
  title "Draft Post"
  slug "draft-post"
  body "This text is not visible"
  created_at Time.parse("2018-04-15 11:00 +0000")
  updated_at Time.parse("2018-04-15 11:00 +0000")
  status "draft"
end
