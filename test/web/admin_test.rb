# frozen_string_literal: true

require File.expand_path("web_test_helper", __dir__)

class AdminTest < TwinCamIntegrationTest
  def setup
    super

    Fabricate(:administrator)
  end

  def test_root_redirects_to_login_when_not_logged_in
    visit("/admin")

    assert_equal("/login", page.current_path)
    assert(page.has_field?("login"))
    assert(page.has_field?("password"))
  end

  def test_admin_panel_link_shows_up_on_login
    login
    visit("/admin")

    assert(page.has_link?("Admin Panel", href: "/admin"))
  end

  def test_root_shows_page_when_logged_in
    login
    visit("/admin")

    assert_equal("/admin", page.current_path)
    refute(page.has_text?("Please login to continue"))
    assert(page.has_link?("Posts", href: "/admin/posts"))
    assert(page.has_link?("Pages", href: "/admin/pages"))
  end

  def test_shows_new_comments_since_last_log_in
    skip("Not yet implemented, requires additional changes in Rodauth")
    login
    visit("/admin")

    assert(page.has_text?("New comments since last login: 1."))
  end
end
