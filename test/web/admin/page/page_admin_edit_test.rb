# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PageAdminEditTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @page = Fabricate(:privacy_policy_page, author_id: user.id)
    login

    visit("/admin/pages/privacy-policy/edit")
  end

  def teardown
    super

    %i[pages users].each { |t| DB.from(t).delete }
  end

  def test_can_update_page_title
    assert(page.has_field?("page[title]"))

    fill_in("page[title]", with: "Completely New title")
    click_link_or_button("Submit")
    @page.reload

    assert_equal("Completely New title", @page.title)
  end

  def test_can_update_page_status
    assert(page.has_field?("page[status]"))

    select("Published", from: "page[status]")
    click_link_or_button("Submit")
    @page.reload

    assert_equal("published", @page.status)
  end

  def test_can_update_page_body
    assert(page.has_field?("page[title]"))

    fill_in("page[body]", with: "This is the new body")
    click_link_or_button("Submit")
    @page.reload

    assert_equal("This is the new body", @page.body)
  end

  def test_cannot_update_page_slug
    assert(page.has_field?("page[title]"))

    fill_in("page[title]", with: "New title")
    click_link_or_button("Submit")

    assert_equal("privacy-policy", @page.slug)
  end

  def test_there_is_a_link_to_back_to_edit_page
    fill_in("page[title]", with: "New title")
    click_link_or_button("Submit")

    assert(page.has_link?("back to edit",
      href: "/admin/pages/privacy-policy/edit"))
  end

  # Negative cases
  def test_cannot_set_title_to_nil
    assert(page.has_field?("page[title]"))

    fill_in("page[title]", with: "")
    click_link_or_button("Submit")

    assert_equal("privacy policy", @page.title)
  end

  def test_trying_a_non_existent_page_raises_an_error
    visit("/admin/pages/non-existent-page/edit")
    assert(page.has_text?("404: Not Found!"))
  end

  def test_cannot_set_body_to_nil
    assert(page.has_field?("page[title]"))

    fill_in("page[body]", with: "")
    click_link_or_button("Submit")

    assert_equal("https://example.com[privacy policy!]",
      @page.body)
  end
end
