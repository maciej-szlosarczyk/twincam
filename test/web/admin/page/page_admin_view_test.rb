# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PageAdminViewTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @page = Fabricate(:privacy_policy_page, author_id: user.id)
    login
  end

  def test_show_page_preview_for_existing_posts
    visit("/admin/pages/privacy-policy")

    assert(page.has_css?("h2.page-title"))
    assert_text("privacy policy!")
  end

  def test_redirects_to_404_for_made_up_values
    visit("/admin/pages/hello-world-from-here")

    refute(page.has_css?("h2.page-title"))
    refute(page.has_text?("hello world!"))
    assert_text("404: Not Found!")
  end
end
