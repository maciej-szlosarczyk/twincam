# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PageAdminCreateTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @existing_page = Fabricate(:privacy_policy_page, author_id: user.id)
    login

    visit("/admin/pages/new")
  end

  def test_can_create_page_with_existing_title
    fill_in("page[title]", with: "hello world!")
    fill_in("page[body]", with: "hello world!")
    click_link_or_button("Submit")

    assert_text("hello world!")
  end

  def test_can_create_page_with_unique_title
    fill_in("page[title]", with: "Unique Title")
    fill_in("page[body]", with: "hello world!")
    click_link_or_button("Submit")

    assert_text("Unique Title")
  end

  def test_cannot_create_page_with_empty_title
    fill_in("page[body]", with: "hello world!")
    click_link_or_button("Submit")

    message =
      page.find("#page_title").native.attribute("validationMessage")

    assert_equal("Please fill out this field.", message)
  end

  def test_can_create_page_with_unique_body
    fill_in("page[title]", with: "hello world!")
    fill_in("page[body]", with: "Unique body")
    click_link_or_button("Submit")

    assert_text("Unique body")
  end

  def test_can_create_page_with_existing_body
    page_body = "https://example.com[hello world!]"

    fill_in("page[title]", with: "hello world!")
    fill_in("page[body]", with: page_body)
    click_link_or_button("Submit")
    assert(page.has_link?("hello world!", href: "https://example.com"))
  end

  def test_cannot_create_page_with_empty_body
    fill_in("page[title]", with: "hello world!")
    click_link_or_button("Submit")

    message =
      page.find("#page_body").native.attribute("validationMessage")

    assert_equal("Please fill out this field.", message)
  end
end
