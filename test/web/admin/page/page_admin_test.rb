# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PageAdminTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    %i[privacy_policy_page second_page draft_page].each do |item|
      Fabricate(item, author_id: user.id)
    end

    login
    visit("/admin/pages")
  end

  def test_admin_displays_list_of_page
    titles = find_all("td.page-title")
    refute(titles.empty?)

    # Page 3 (still draft):
    # title:      Draft Page
    # body:       This text is not visible
    # created_at: 2018-04-15 11:00 +0000
    # updated_at: 2018-04-15 11:00 +0000
    # status:     draft
    assert_equal("Draft Page", titles[0].text)

    # Page 2 (newer):
    # title:      Second page!
    # body:       This is just another page on this website. Move along.
    # created_at: 2018-04-10 11:00 +0000
    # updated_at: 2018-04-10 11:00 +0000
    # status:     published
    assert_equal("Second page", titles[1].text)

    # Page 1 (older):
    # title:      hello world!
    # body:       hello world!
    # created_at: 2018-04-01 11:00 +0000
    # updated_at: 2018-04-01 11:00 +0000
    # status:     published
    assert_equal("privacy policy", titles[2].text)
  end

  def test_list_items_are_link
    assert(page.has_link?("Second page",
      href: "/admin/pages/second-page/edit"))
    assert(page.has_link?("privacy policy",
      href: "/admin/pages/privacy-policy/edit"))
    assert(page.has_link?("Draft Page",
      href: "/admin/pages/draft-page/edit"))
  end
end
