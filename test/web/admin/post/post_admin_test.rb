# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PostAdminTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    %i[hello_world_post second_post draft_post].each do |item|
      Fabricate(item, author_id: user.id)
    end

    login
    visit("/admin/posts")
  end

  def test_admin_displays_list_of_post
    titles = find_all("td.post-title")
    refute(titles.empty?)

    # Post 3 (still draft):
    # title:      Draft Post
    # body:       This text is not visible
    # created_at: 2018-04-15 11:00 +0000
    # updated_at: 2018-04-15 11:00 +0000
    # status:     draft
    assert_equal("Draft Post", titles[0].text)

    # Post 2 (newer):
    # title:      Second post!
    # body:       I am here, waiting for you second post to materialize.
    #             And it has been written the second day.
    # created_at: 2018-04-10 11:00 +0000
    # updated_at: 2018-04-10 11:00 +0000
    # status:     published
    assert_equal("Second post!", titles[1].text)

    # Post 1 (older):
    # title:      hello world!
    # body:       hello world!
    # created_at: 2018-04-01 11:00 +0000
    # updated_at: 2018-04-01 11:00 +0000
    # status:     published
    assert_equal("hello world!", titles[2].text)
  end

  def test_posts_are_paginated
    @previous_pagination = ENV["TWIN_CAM_PAGINATION"]
    ENV["TWIN_CAM_PAGINATION"] = "1"
    visit("/admin/posts")

    titles = find_all("td.post-title")
    assert_equal(1, titles.count)

    ENV["TWIN_CAM_PAGINATION"] = @previous_pagination
  end

  def test_pagination_contains_links
    @previous_pagination = ENV["TWIN_CAM_PAGINATION"]
    ENV["TWIN_CAM_PAGINATION"] = "1"
    visit("/admin/posts")

    assert(page.has_link?("Next", href: "/admin/posts/page/2"))
    click_link_or_button("Next")

    assert(page.has_link?("Previous", href: "/admin/posts/page/1"))
    assert(page.has_link?("Next", href: "/admin/posts/page/3"))

    ENV["TWIN_CAM_PAGINATION"] = @previous_pagination
  end

  def test_list_items_are_links
    assert(page.has_link?("Second post!",
      href: "/admin/posts/2018/04/second-post/edit"))
    assert(page.has_link?("hello world!",
      href: "/admin/posts/2018/04/hello-world/edit"))
    assert(page.has_link?("Draft Post",
      href: "/admin/posts/2018/04/draft-post/edit"))
  end
end
