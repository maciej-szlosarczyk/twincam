# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PostAdminCreateTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    login

    visit("/admin/posts/new")
  end

  def test_can_create_post_with_existing_title
    fill_in("post[title]", with: "hello world!")
    fill_in("post[body]", with: "hello world!")
    click_link_or_button("Submit")

    assert_text("hello world!")
  end

  def test_can_create_post_with_unique_title
    fill_in("post[title]", with: "Unique Title")
    fill_in("post[body]", with: "hello world!")
    click_link_or_button("Submit")

    assert_text("Unique Title")
  end

  def test_cannot_create_post_with_empty_title
    fill_in("post[body]", with: "hello world!")
    click_link_or_button("Submit")

    message =
      page.find("#post_title").native.attribute("validationMessage")

    assert_equal("Please fill out this field.", message)
  end

  def test_can_create_post_with_unique_body
    fill_in("post[title]", with: "hello world!")
    fill_in("post[body]", with: "Unique body")
    click_link_or_button("Submit")

    assert_text("Unique body")
  end

  def test_can_create_post_with_existing_body
    post_body = "https://example.com[hello world!]"

    fill_in("post[title]", with: "hello world!")
    fill_in("post[body]", with: post_body)
    click_link_or_button("Submit")
    assert(page.has_link?("hello world!", href: "https://example.com"))
  end

  def test_cannot_create_post_with_empty_body
    fill_in("post[title]", with: "hello world!")
    click_link_or_button("Submit")

    message =
      page.find("#post_body").native.attribute("validationMessage")

    assert_equal("Please fill out this field.", message)
  end
end
