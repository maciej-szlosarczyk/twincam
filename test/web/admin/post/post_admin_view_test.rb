# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PostAdminViewTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    login
  end

  def test_show_post_preview_for_existing_posts
    visit("/admin/posts/2018/04/hello-world")

    assert(page.has_css?("h2.post-title"))
    assert_text("hello world!")
  end

  def test_redirects_to_404_for_made_up_values
    visit("/admin/posts/2018/04/hello-world-from-here")

    refute(page.has_css?("h2.post-title"))
    refute(page.has_text?("hello world!"))
    assert_text("404: Not Found!")
  end
end
