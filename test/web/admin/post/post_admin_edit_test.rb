# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class PostAdminEditTest < TwinCamIntegrationTest
  def setup
    super

    @user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: @user.id)

    login

    visit("/admin/posts/2018/04/hello-world/edit")
  end

  def teardown
    super

    %i[posts users].each { |t| DB.from(t).delete }
  end

  def test_can_update_post_title
    assert(page.has_field?("post[title]"))

    fill_in("post[title]", with: "Completely New title")
    click_link_or_button("Submit")
    @post.reload

    assert_equal("Completely New title", @post.title)
  end

  def test_can_update_post_status
    assert(page.has_field?("post[status]"))

    select("Published", from: "post[status]")
    click_link_or_button("Submit")
    @post.reload

    assert_equal("published", @post.status)
  end

  def test_can_update_post_body
    assert(page.has_field?("post[title]"))

    fill_in("post[body]", with: "This is the new body")
    click_link_or_button("Submit")
    @post.reload

    assert_equal("This is the new body", @post.body)
  end

  def test_cannot_update_post_slug
    assert(page.has_field?("post[title]"))

    fill_in("post[title]", with: "New title")
    click_link_or_button("Submit")

    assert_equal("hello-world", @post.slug)
  end

  def test_there_is_a_link_to_back_to_edit_page
    fill_in("post[title]", with: "New title")
    click_link_or_button("Submit")

    assert(page.has_link?("back to edit",
      href: "/admin/posts/2018/04/hello-world/edit"))
  end

  # Negative cases
  def test_cannot_set_title_to_nil
    assert(page.has_field?("post[title]"))

    fill_in("post[title]", with: "")
    click_link_or_button("Submit")

    assert_equal("hello world!", @post.title)
  end

  def test_trying_a_non_existent_post_raises_an_error
    visit("/admin/posts/2018/04/non-existent-post/edit")
    assert(page.has_text?("404: Not Found!"))
  end

  def test_cannot_set_body_to_nil
    assert(page.has_field?("post[title]"))

    fill_in("post[body]", with: "")
    click_link_or_button("Submit")

    assert_equal("https://example.com[hello world!]", @post.body)
  end
end
