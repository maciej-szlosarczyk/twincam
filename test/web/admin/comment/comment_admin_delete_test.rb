# frozen_string_literal: true

require File.expand_path("../../web_test_helper", __dir__)

class CommentAdminDeleteTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @comment = Fabricate(:first_comment, post_id: @post.id)

    login
  end

  def teardown
    super

    %i[comments posts users].each { |t| DB.from(t).delete }
  end

  def test_administrator_can_delete_comment
    visit("/admin/posts/2018/04/hello-world/edit")
    click_link_or_button("Delete comment")

    assert(page.has_text?("Comment deleted."))
    refute(@comment.exists?)
  end
end
