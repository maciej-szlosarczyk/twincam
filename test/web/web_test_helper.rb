# frozen_string_literal: true

ENV["RACK_ENV"] = "test"
require File.expand_path("../../twin_cam", __dir__)
unless DB.opts[:database].end_with?("test")
  raise "test database doesn't end with test"
end

require "capybara"
require "capybara/dsl"
require "rack/test"
require "selenium/webdriver"

require File.expand_path("../minitest_helper", __dir__)

Capybara.app = TwinCam.freeze.app
Capybara.save_path = "tmp/capybara"

module Helpers
  module LoginHelper
    # Login test helper, accepted the following options hash:
    # opts[:path]     - String path, defaults to "/login"
    # opts[:login]    - String login, defaults to "/test@email.com"
    # opts[:password] - String password, defaults to "password"
    def login(opts = {})
      visit(opts[:path] || "/login")
      fill_in("Login", with: opts[:login] || "test@email.com")
      fill_in("Password", with: opts[:password] || "password")
      click_button("Login")
    end
  end
end

# Base class for integration tests with javascript, whenever you need to write
# a new one:
# class MyNewIntegrationTest < TwinCamIntegrationTest
# end
class TwinCamIntegrationTest < TwinCamTest
  include Rack::Test::Methods
  include Capybara::DSL
  include Helpers::LoginHelper

  Capybara.register_driver(:chrome) do |app|
    options = ::Selenium::WebDriver::Chrome::Options.new

    options.add_argument("--headless")
    options.add_argument("--no-sandbox")
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--window-size=1400,1400")

    Capybara::Selenium::Driver.new(app, browser: :chrome, options: options)
  end

  Capybara.register_server(:silent_puma) do |app, port, _host|
    require "rack/handler/puma"
    Rack::Handler::Puma.run(app, Port: port, Threads: "0:2", Silent: true)
  end

  def app
    Capybara.app
  end

  def setup
    super

    Capybara.current_driver = :chrome
    Capybara.server = :silent_puma
  end

  def teardown
    super

    Capybara.reset_sessions!
    Capybara.use_default_driver

    # Delete all objects after tests are done
    %i[comments pages posts users].each do |t|
      DB.from(t).delete
      DB.from(Sequel[:audit][t]).delete
    end
  end
end
