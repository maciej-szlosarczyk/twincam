# frozen_string_literal: true

require File.expand_path("web_test_helper", __dir__)

class CommentsTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)

    %i[hello_world_post second_post draft_post].each do |item|
      Fabricate(item, author_id: user.id)
    end

    # Avoid being caught by spam protection
    @original_spam_timeout = ENV["TWIN_CAM_SPAM_TIMEOUT"]
    ENV["TWIN_CAM_SPAM_TIMEOUT"] = "0"
  end

  def teardown
    super

    ENV["TWIN_CAM_SPAM_TIMEOUT"] = @original_spam_timeout

    %i[comments posts users].each { |t| DB.from(t).delete }
  end

  def test_i_can_create_a_comment_as_visitor
    visit("/blog/2018/04/hello-world")
    assert(page.has_field?("comment[body]"))
    assert(page.has_field?("comment[email]"))

    fill_in("comment[body]", with: "Some comment text")
    fill_in("comment[email]", with: "email@example.com")
    fill_in("comment[name]", with: "Johnny Commenter")

    click_link_or_button("Submit")

    assert_equal(1, Comment.where(name: "Johnny Commenter").count)
    assert(page.has_text?("Your comment was saved. Thank you."))
  end

  def test_i_can_create_a_comment_as_administrator_and_email_and_name_is_filled
    login

    visit("/blog/2018/04/hello-world")
    assert(page.has_field?("comment[email]", with: "test@email.com"))
    assert(page.has_field?("comment[name]", with: "Test Admin"))

    fill_in("comment[body]", with: "Some comment text")

    click_link_or_button("Submit")
    assert_equal(1, Comment.where(name: "Test Admin").count)
    assert(page.has_text?("Your comment was saved. Thank you."))
  end

  def test_comments_require_email
    visit("/blog/2018/04/hello-world")
    fill_in("comment[body]", with: "Some comment text")

    click_link_or_button("Submit")
    refute(page.has_text?("Some comment text"))
  end

  def test_comments_require_body
    visit("/blog/2018/04/hello-world")
    fill_in("comment[email]", with: "email@example.com")
    click_link_or_button("Submit")
    refute(page.has_text?("email@example.com"))
  end

  def test_comments_are_csrf_protected
    post("/blog/2018/04/hello-world/comments")
    response_body = last_response.body
    response_status = last_response.status

    assert(response_body.empty?)
    assert_equal(403, response_status)
  end
end
