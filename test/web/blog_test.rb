# frozen_string_literal: true

require File.expand_path("web_test_helper", __dir__)

# Blog tests
class BlogTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)

    %i[hello_world_post second_post draft_post].each do |item|
      Fabricate(item, author_id: user.id)
    end
  end

  def test_root_has_list_of_published_posts_ordered_by_date_in_reverse
    visit("/")

    titles = find_all("h2.post-title")
    bodies = find_all("div.post-content")

    # Post 2 (newer):
    # title:      Second post!
    # body:       I am here, waiting for you second post to materialize.
    #             And it has been written the second day.
    # created_at: 2018-04-10 11:00 +0000
    # updated_at: 2018-04-10 11:00 +0000
    assert_equal("Second post!", titles[0].text)

    expected_body = <<~TEXT.chop
      app.rb
      require 'sinatra'

      get '/hi' do
      \"Hello World!\"
      end
    TEXT
    assert_equal(expected_body, bodies[0].text)

    # Post 1 (older):
    # title:      hello world!
    # body:       hello world!
    # created_at: 2018-04-01 11:00 +0000
    # updated_at: 2018-04-01 11:00 +0000
    assert_equal("hello world!", titles[1].text)
    assert_equal("hello world!", bodies[1].text)

    # Post 3 (draft):
    # title:      Draft Post
    # status:     draft
    refute(page.has_text?("Draft Post"))
  end

  def test_root_is_paginated
    @previous_pagination = ENV["TWIN_CAM_PAGINATION"]
    ENV["TWIN_CAM_PAGINATION"] = "1"
    visit("/")

    titles = find_all("h2.post-title")
    bodies = find_all("div.post-content")

    assert_equal(1, titles.count)
    assert_equal(1, bodies.count)

    ENV["TWIN_CAM_PAGINATION"] = @previous_pagination
  end

  def test_pagination_contains_links
    @previous_pagination = ENV["TWIN_CAM_PAGINATION"]
    ENV["TWIN_CAM_PAGINATION"] = "1"
    visit("/")

    assert(page.has_link?("Next", href: "/blog/page/2"))
    click_link_or_button("Next")

    assert(page.has_link?("Previous", href: "/blog/page/1"))
    assert(page.has_link?("Next", href: "/blog/page/3"))

    ENV["TWIN_CAM_PAGINATION"] = @previous_pagination
  end

  def test_blog_root_works_both_with_and_without_slash
    visit("/blog")
    assert(page.has_link?(href: "https://example.com"))

    visit("/blog/")
    assert(page.has_link?(href: "https://example.com"))
  end

  def test_posts_are_html_safe
    visit("/blog/2018/04/hello-world")
    assert(page.has_link?(href: "https://example.com"))
  end

  def test_get_single_blog_post_by_yyyy_mm_slug
    # Post 1 (older):
    # title:      hello world!
    # body:       hello world!
    # slug:       hello-world
    # created_at: 2018-04-01 11:00 +0000
    # updated_at: 2018-04-01 11:00 +0000
    visit("/blog/2018/04/hello-world")
    titles = find_all("h2.post-title")
    bodies = find_all("div.post-content")

    assert_equal("hello world!", titles[0].text)
    assert_equal("hello world!", bodies[0].text)
  end

  def test_has_meta_title_tag
    visit("/blog/2018/04/hello-world")
    within("head", visible: false) do
      title = page.find("title", visible: false)
      assert(title)
    end
  end

  def test_comments_links_do_not_contain_number_of_comments
    visit("/blog/")
    assert(page.has_link?("Contact me about this post", href: "/blog/2018/04/hello-world"))
  end

  def test_cannot_access_draft_posts_even_if_i_have_url
    visit("/blog/2018/04/draft-post")
    assert_text("404: Not Found!")
  end

  def test_get_404_instead_of_posts
    visit("/blog/2018/04/non_existent")
    assert_text("404: Not Found!")

    visit("/blog/2018/12/non_existent")
    assert_text("404: Not Found!")
  end
end
