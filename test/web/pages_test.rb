# frozen_string_literal: true

require File.expand_path("web_test_helper", __dir__)

# Test class for standalone pages.
class PagesTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)
    %i[privacy_policy_page second_page draft_page].each do |item|
      Fabricate(item, author_id: user.id)
    end
  end

  def test_first_public_page_is_displayed
    visit("/privacy-policy")
    title = page.find("h2.page-title")
    content = page.find("div.page-content")

    assert_equal("privacy policy", title.text)
    assert_equal("privacy policy!", content.text)
  end

  def test_has_meta_title_tag
    visit("/privacy-policy")
    within("head", visible: false) do
      title = page.find("title", visible: false)
      assert(title)
    end
  end

  def test_second_public_page_is_displayed
    visit("/second-page")
    title = page.find("h2.page-title")
    content = page.find("div.page-content")

    assert_equal("Second page", title.text)
    assert_equal("This is just another page on this website. Move along.",
      content.text)
  end

  def test_draft_pages_return_404
    visit("draft-page")
    assert_text("404: Not Found!")
  end
end
