# frozen_string_literal: true

require File.expand_path("web_test_helper", __dir__)

class CommentsSpamTest < TwinCamIntegrationTest
  def setup
    super

    user = Fabricate(:administrator)

    %i[hello_world_post second_post draft_post].each do |item|
      Fabricate(item, author_id: user.id)
    end
  end

  def teardown
    super

    %i[comments posts users].each { |t| DB.from(t).delete }
  end

  def test_robot_comments_are_rejected
    visit("/blog/2018/04/hello-world")

    fill_in("comment[body]", with: "Some comment text")
    fill_in("comment[email]", with: "email@example.com")
    fill_in("comment[name]", with: "Johnny Commenter")

    click_link_or_button("Submit")

    assert(page.has_text?("I am not even sorry..."))

    error_text = "Your comment submitting tempo indicates " \
                 "that you are a spam bot"
    assert_text(error_text)
  end

  def test_administrator_bypasses_the_check
    login

    visit("/blog/2018/04/hello-world")
    assert(page.has_field?("comment[email]", with: "test@email.com"))
    assert(page.has_field?("comment[name]", with: "Test Admin"))

    fill_in("comment[body]", with: "Some comment text")

    click_link_or_button("Submit")
    assert_equal(1, Comment.where(name: "Test Admin").count)
  end
end
