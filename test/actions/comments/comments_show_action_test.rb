# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class CommentsShowActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @comment = Fabricate(:first_comment, post_id: @post.id)
  end

  def test_returns_comment_if_exists
    params = {comment_id: @comment.id}
    instance = Actions::Comments::Show.new(params)

    result = instance.call

    assert_equal(@comment, result)
  end

  def test_returns_nil_if_not_found
    # Zero is never assigned to pk, so it's safe to use in this context
    params = {comment_id: 0}
    instance = Actions::Comments::Show.new(params)

    result = instance.call

    refute(result)
  end
end
