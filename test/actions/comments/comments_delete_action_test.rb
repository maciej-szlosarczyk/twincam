# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class CommentsDeleteActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @comment = Fabricate(:first_comment, post_id: @post.id)
  end

  def test_deletes_comment_and_returns_post
    action = Actions::Comments::Delete.new(@comment)
    return_value = action.call

    refute(@comment.exists?)
    assert(return_value.is_a?(Post))
  end

  def test_falls_on_its_face_when_garbage_is_inserted
    params = {comment_id: @comment.id}

    action = Actions::Comments::Delete.new(params)

    assert_raises(ArgumentError) do
      action.call
    end
  end
end
