# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class CommentsNewActionTest < TwinCamActionTest
  def setup
    super

    @user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: @user.id)
  end

  def test_return_comment_type
    action = Actions::Comments::New.new(@post.id)
    comment = action.call

    assert(comment.is_a?(Comment))
  end

  def test_returns_no_email_when_user_id_is_missing
    action = Actions::Comments::New.new(@post.id)
    comment = action.call

    assert_equal(@post.id, comment.post_id)
    refute(comment[:email])
  end

  def test_returns_user_email_when_user_id_is_supplied
    action = Actions::Comments::New.new(@post.id, @user.id)
    comment = action.call

    assert_equal(@post.id, comment.post_id)
    assert_equal("test@email.com", comment[:email])
  end

  def test_returns_user_name_when_user_id_is_supplied
    action = Actions::Comments::New.new(@post.id, @user.id)
    comment = action.call

    assert_equal(@post.id, comment.post_id)
    assert_equal("Test Admin", comment.name)
  end
end
