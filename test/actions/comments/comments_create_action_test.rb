# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class CommentsCreateActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
  end

  def test_comment_creation_returns_comment_back_when_valid
    params = {"comment" => {"post_id" => @post.id.to_s,
                            "email" => "email@example.com",
                            "body" => "Some comment text",
                            "name" => "Johnny Appleseed"}}
    action = Actions::Comments::Create.new(params)
    comment = action.call

    assert(comment.is_a?(Comment))
    assert(comment.valid?)
  end

  def test_comment_creation_returns_result_back_when_invalid
    params = {"comment" => {"email" => "email@example.com",
                            "body" => "Some comment text"}}
    action = Actions::Comments::Create.new(params)
    comment = action.call

    refute(comment.is_a?(Comment))
    assert(comment.is_a?(Dry::Validation::Result))
  end

  def test_does_not_allow_for_non_permitted_fields
    date = Time.parse("2018-04-01T11:00:00+0000")
    params = {"comment" => {"post_id" => @post.id.to_s, "email" => "email@example.com",
                            "body" => "Some comment text", "name" => "Johnny Appleseed",
                            "created_at" => date}}
    action = Actions::Comments::Create.new(params)
    comment = action.call

    assert(comment.is_a?(Comment))
    assert(comment.valid?)
    refute_equal(date, comment.created_at)
  end
end
