# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PagesNewActionTest < TwinCamActionTest
  def setup
    super

    @user = Fabricate(:administrator)
  end

  def test_returns_page_type
    action = Actions::Pages::New.new(@user.id)
    pages = action.call

    assert(pages.is_a?(Page))
  end
end
