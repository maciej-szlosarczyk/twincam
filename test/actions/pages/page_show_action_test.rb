# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PageShowActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @page = Fabricate(:privacy_policy_page, author_id: user.id)
  end

  def test_returns_pages_according_to_passed_params
    params = {status: "published", slug: "privacy-policy"}
    instance = Actions::Pages::Show.new(params)

    result = instance.call

    assert_equal(@page, result)
  end

  def test_returns_page_when_passed_status_is_nil
    params = {slug: "privacy-policy"}

    instance = Actions::Pages::Show.new(params)

    result = instance.call

    assert_equal(@page, result)
  end
end
