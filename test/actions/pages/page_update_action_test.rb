# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PagesUpdateActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @page = Fabricate(:privacy_policy_page, author_id: user.id)
    @params = {
      "page" => {
        "author_id" => @page.author_id.to_s,
        "title" => "Shiny new title",
        "body" => "Great new body",
        "status" => "published"
      }
    }
  end

  def test_page_update_returns_page_back_when_valid
    action = Actions::Pages::Update.new(@page, @params)
    page = action.call

    assert(page.is_a?(Page))
    assert(page.valid?)
  end

  def test_returns_result_back_when_invalid
    params = {"page" => {"title" => 123, "status" => "some string"}}

    action = Actions::Pages::Update.new(@page, params)
    page = action.call

    refute(page.is_a?(Page))
    assert(page.is_a?(Dry::Validation::Result))
  end
end
