# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PagesCreateActionTest < TwinCamActionTest
  def setup
    super

    @user = Fabricate(:administrator)
  end

  def test_page_creation_returns_page_back_when_valid
    params = {"page" => {"body" => "Some page text",
                         "title" => "Some page",
                         "author_id" => @user.id}}
    action = Actions::Pages::Create.new(params)
    page = action.call

    assert(page.is_a?(Page))
    assert(page.valid?)
  end

  def test_page_creation_returns_result_back_when_invalid
    params = {"page" => {"email" => "email@example.com",
                         "body" => "Some page text"}}
    action = Actions::Pages::Create.new(params)
    page = action.call

    refute(page.is_a?(Page))
    assert(page.is_a?(Dry::Validation::Result))
  end

  def test_does_not_allow_for_non_permitted_fields
    date = Time.parse("2018-04-01T11:00:00+0000")
    params = {"page" => {"body" => "Some page text",
                         "title" => "Some page",
                         "author_id" => @user.id,
                         "created_at" => "now"}}
    action = Actions::Pages::Create.new(params)
    page = action.call

    assert(page.is_a?(Page))
    assert(page.valid?)
    refute_equal(date, page.created_at)
  end
end
