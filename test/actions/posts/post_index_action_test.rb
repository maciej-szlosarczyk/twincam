# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PostsIndexActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)

    %i[hello_world_post second_post draft_post].each do |item|
      Fabricate(item, author_id: user.id)
    end
  end

  def test_call_returns_the_result_object
    action = Actions::Posts::Index.new(10, 1)
    result = action.call

    assert(result.is_a?(OpenStruct))
    assert_equal(1, result.page)
    assert_equal(1, result.total_pages)
  end

  def test_call_returns_all_posts_if_no_params_are_given
    action = Actions::Posts::Index.new(10, 1)
    result = action.call

    assert_equal(3, result.records.count)
  end

  def test_call_returns_only_published_post_if_the_parameter_is_given
    action = Actions::Posts::Index.new(10, 1, status: "published")
    result = action.call

    assert_equal(2, result.records.count)
  end

  def test_call_returns_paginated_post
    action = Actions::Posts::Index.new(2, 1, status: "published")
    result = action.call

    assert_equal(2, result.records.count)
  end
end
