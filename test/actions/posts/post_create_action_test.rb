# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PostsCreateActionTest < TwinCamActionTest
  def setup
    super

    @user = Fabricate(:administrator)
  end

  def test_post_creation_returns_post_back_when_valid
    params = {"post" => {"body" => "Some post text",
                         "title" => "Some post",
                         "author_id" => @user.id}}
    action = Actions::Posts::Create.new(params)
    post = action.call

    assert(post.is_a?(Post))
    assert(post.valid?)
  end

  def test_post_creation_returns_result_back_when_invalid
    params = {"post" => {"email": "email@example.com",
                         "body": "Some post text"}}
    action = Actions::Posts::Create.new(params)
    post = action.call

    refute(post.is_a?(Post))
    assert(post.is_a?(Dry::Validation::Result))
  end

  def test_does_not_allow_for_non_permitted_fields
    date = Time.parse("2018-04-01T11:00:00+0000")
    params = {"post" => {"body": "Some post text",
                         "title": "Some post",
                         "author_id": @user.id,
                         "created_at": date}}
    action = Actions::Posts::Create.new(params)
    post = action.call

    assert(post.is_a?(Post))
    assert(post.valid?)
    refute_equal(date, post.created_at)
  end
end
