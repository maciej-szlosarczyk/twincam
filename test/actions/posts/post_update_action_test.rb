# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PostsUpdateActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @params = {
      "post" => {
        "author_id" => @post.author_id,
        "title" => "Shiny new title",
        "body" => "Great new body",
        "status" => "published"
      }
    }
  end

  def test_post_update_returns_post_back_when_valid
    action = Actions::Posts::Update.new(@post, @params)
    post = action.call

    assert(post.is_a?(Post))
    assert(post.valid?)
  end

  def test_returns_result_back_when_invalid
    params = {"title": 123, "status": "some string"}

    action = Actions::Posts::Update.new(@post, params)
    post = action.call

    refute(post.is_a?(Post))
    assert(post.is_a?(Dry::Validation::Result))
  end
end
