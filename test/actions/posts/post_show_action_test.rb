# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PostShowActionTest < TwinCamActionTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @draft = Fabricate(:draft_post, author_id: user.id)
  end

  def test_returns_post_with_passed_in_status
    params = {status: "published",
              slug: "hello-world",
              year: "2018",
              month: "04"}
    instance = Actions::Posts::Show.new(params)

    result = instance.call

    assert_equal(@post, result)
  end

  def test_raises_an_error_when_status_is_garbage
    params = {status: "foo",
              slug: "hello-world",
              year: "2018",
              month: "04"}
    instance = Actions::Posts::Show.new(params)

    assert_raises Sequel::DatabaseError do
      instance.call
    end
  end

  def test_returns_also_drafts_when_status_is_not_passed
    params = {slug: "draft-post",
              year: "2018",
              month: "04"}

    instance = Actions::Posts::Show.new(params)

    result = instance.call

    assert_equal(@draft, result)
  end
end
