# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class PostsNewActionTest < TwinCamActionTest
  def setup
    super

    @user = Fabricate(:administrator)
  end

  def test_returns_post_type
    action = Actions::Posts::New.new(@user.id)
    posts = action.call

    assert(posts.is_a?(Post))
  end
end
