# frozen_string_literal: true

require File.expand_path("../action_test_helper", __dir__)

class UsersCreateActionTest < TwinCamActionTest
  def setup
    super
  end

  def test_user_creation_returns_user_back_when_valid
    params = {"name": "Johnny Appleseed",
              "email": "johnny@appleseed.com",
              "password": "password"}
    action = Actions::Users::Create.new(params)
    user = action.call

    assert(user.is_a?(User))
    assert(user.valid?)
  end

  def test_user_creation_returns_result_back_when_invalid
    params = {"name": 123, "email": "some string"}

    action = Actions::Users::Create.new(params)
    user = action.call

    refute(user.is_a?(User))
    assert(user.is_a?(Dry::Validation::Result))
  end

  def test_does_not_allow_for_non_permitted_fields
    date = Time.parse("2018-04-01T11:00:00+0000")
    params = {"name": "Johnny Appleseed",
              "email": "johnny@appleseed.com",
              "password": "password",
              "created_at": date}
    action = Actions::Users::Create.new(params)
    user = action.call

    assert(user.is_a?(User))
    assert(user.valid?)
    refute_equal(date, user.created_at)
  end
end
