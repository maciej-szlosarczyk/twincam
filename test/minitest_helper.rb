# frozen_string_literal: true

# Configure test coverage
require "simplecov"
require "simplecov-console"

unless ENV["SIMPLE_COVERAGE_REPORTER"]
  SimpleCov.formatter = SimpleCov::Formatter::Console
end

SimpleCov.start do
  coverage_dir("tmp/coverage")
  add_filter "test/"
  add_filter "lib/.env.rb"
  add_filter "lib/db.rb"
  add_filter "lib/models.rb"

  minimum_coverage(90)
  minimum_coverage_by_file(75)
end

require "minitest/autorun"
require "minitest/hooks/default"
require "minitest/reporters"

require "sequel"
require "timecop"

# Load the whole app
require File.expand_path("../lib/app_container", __dir__)

# Fake objects
require "fabrication"
require File.expand_path("fabricators", __dir__)

# Base test class, both TwinCamIntegrationTest and
# TwinCamModelTest inherit from it.
class TwinCamTest < Minitest::Test
  include Minitest::Hooks
  Minitest::Reporters.use!(
    [Minitest::Reporters::DefaultReporter.new(color: true)]
  )
end
