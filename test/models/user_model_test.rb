# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)

# Tests for user model
class UserModelTest < TwinCamModelTest
  def setup
    super

    Timecop.freeze("2018-04-01 11:00 +0000")
    @params = {
      email: "test@email.com",
      password: "P2ssword",
      name: "Test Admin"
    }
  end

  def teardown
    super
    Timecop.return
  end

  def test_automatically_assigned_created_at_timestamp
    user = User.new
    user.valid?
    assert_equal(Time.parse("2018-04-01 11:00:00 +0000"), user.created_at)
  end

  def test_create_user_with_valid_attributes_creates_user
    user = User.new(@params)
    user.save

    assert_equal("test@email.com", user.email)
  end

  def test_changing_password_changes_password_digest
    user = User.new(@params)
    digest = user.password_digest
    user.password = "Some new password"

    refute_equal(digest, user.password_digest)
  end

  def test_cannot_create_with_blank_email
    user = User.new(password: "P2ssword", name: "Test Admin")

    refute(user.valid?)
    assert_equal({email: ["is not present", "is invalid"]}, user.errors)
  end

  def test_cannot_create_with_invalid_email
    user = User.new(password: "P2ssword", email: "Some text",
                    name: "Test Admin")

    refute(user.valid?)
    assert_equal({email: ["is invalid"]}, user.errors)
  end

  def test_cannot_create_with_blank_password
    user = User.new(email: "email@user.com", name: "Test Admin")

    refute(user.valid?)
    assert_equal({password_digest: ["is not present"]}, user.errors)
  end
end
