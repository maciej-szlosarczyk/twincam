# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)

# Model tests for Post
class PostModelTest < TwinCamModelTest
  def setup
    super

    @admin = Fabricate(:administrator)
    Timecop.freeze("2018-04-01 11:00 +0000")
  end

  def teardown
    super
    Timecop.return
  end

  def test_needs_required_fields_to_create_post
    post = Post.new
    post.validate

    expected_errors_hash = {title: ["is not present"],
                            slug: ["is not present"],
                            author_id: ["is not present", "cannot be empty"],
                            created_at: ["is not present"]}

    assert_equal(%i[title slug author_id created_at], post.errors.keys)
    assert_equal(expected_errors_hash, post.errors)
  end

  def test_creates_universally_accepted_slugs_for_ascii
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")
    post.save
    assert_equal("simple-title", post.slug)
  end

  def test_author_returns_user_instance
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")

    assert_equal(@admin, post.author)
  end

  def test_url_path_returns_nil_if_not_persisted
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")
    refute(post.url_path)
  end

  def test_url_path_returns_dated_string_if_persisted
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")
    post.save
    assert_equal("2018/04/simple-title", post.url_path)
  end

  def test_creates_universally_accepted_slugs_for_polish_and_estonian
    post = Post.new(user: @admin, title: "Chrząszcz Õllekäpp",
                    body: "Some text")
    post.save
    assert_equal("chrz-szcz--llek-pp", post.slug)
  end

  def test_creates_only_unique_slugs
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")
    other_post = Post.new(user: @admin, title: "Simple title",
                          body: "Some text")
    post.save
    other_post.save
    assert_equal("simple-title", post.slug)
    assert_equal("simple-title-1", other_post.slug)
  end

  def test_comment_count_returns_an_integer
    post = Post.new(user: @admin, title: "Simple title", body: "Some text")
    post.save

    assert_equal(0, post.comment_count)
  end
end
