# frozen_string_literal: true

ENV["RACK_ENV"] = "test"
require File.expand_path("../../lib/models", __dir__)
unless DB.opts[:database].end_with?("test")
  raise "test database doesn't end with test"
end

require File.expand_path("../minitest_helper", __dir__)

# Base class for model tests, which should inherit from it:
# MyModelTest < TwinCamModelTest
# Ran in parallel by default.
class TwinCamModelTest < TwinCamTest
  # Keeps all tests in transactions with savepoint.
  def around
    DB.transaction(rollback: :always, savepoint: true, auto_savepoint: true) do
      super
    end
  end

  def around_all
    DB.transaction(rollback: :always) do
      super
    end
  end

  # MiniTest has this awesome ability of running multiple tests at once.
  parallelize_me!
end
