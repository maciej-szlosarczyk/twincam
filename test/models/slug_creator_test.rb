# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)
require File.expand_path("../../lib/slug_creator", __dir__)
require File.expand_path("../../lib/models/post", __dir__)

class SlugCreatorTest < TwinCamModelTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
  end

  def test_creates_universally_accepted_slugs_for_ascii
    slug = SlugCreator.new("Simple title", Post)
    slug.find_unique_slug
    assert_equal("simple-title", slug.result)
  end

  def test_creates_universally_accepted_slugs_for_polish_and_estonian
    slug = SlugCreator.new("Chrząszcz Õllekäpp", Post)
    slug.find_unique_slug
    assert_equal("chrz-szcz--llek-pp", slug.result)
  end

  def test_creates_only_unique_slugs
    slug = SlugCreator.new("Hello World", Post)
    slug.find_unique_slug
    assert_equal("hello-world-1", slug.result)
  end

  def test_injecting_a_non_sluggable_class_raises_an_error
    slug = SlugCreator.new("Hello World", User)
    assert_raises(Sequel::DatabaseError) do
      slug.find_unique_slug
    end
  end
end
