# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)
require File.expand_path("../../lib/application_logger", __dir__)
require "logger"

class ApplicationLoggerTest < TwinCamModelTest
  def setup
    @file_logger = ApplicationLogger::File
    @stdout_logger = ApplicationLogger::StandardOut
  end

  def teardown
    super
  end

  def test_classes_are_singletons
    assert_equal(ApplicationLogger::File, @file_logger)
    assert_equal(ApplicationLogger::StandardOut, @stdout_logger)
  end

  def test_loggers_are_instances_of_logger_class
    assert(@file_logger.logger.is_a?(Logger))
    assert(@stdout_logger.logger.is_a?(Logger))
  end

  def test_classes_instantiate_loggers_only_once
    file_logger = @file_logger.logger
    assert_equal(file_logger, @file_logger.logger)

    stdout_logger = @stdout_logger.logger
    assert_equal(stdout_logger, @stdout_logger.logger)
  end
end
