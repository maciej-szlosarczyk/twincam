# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)

# Model tests for Comment
class CommentModelTest < TwinCamModelTest
  def setup
    super

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @comment = Fabricate(:first_comment, post_id: @post.id)
  end

  def test_needs_required_fields_to_create_comment
    comment = Comment.new
    comment.validate

    assert_equal(%i[post_id body created_at name email_digest],
      comment.errors.keys)
    comment.errors.each do |_k, v|
      assert_equal(["is not present"], v)
    end
  end

  def test_when_a_post_is_deleted_comments_are_deleted
    @post.destroy
    assert_equal(0, Comment.count)
  end

  def test_email_is_saved_but_hex_digest_can_be_used_to_identify
    comment = create_comment("Johnny Appleseed")
    other_comment = create_comment("Not Johnny Appleseed")

    assert_equal(comment.email_digest, other_comment.email_digest)
    assert_equal("example@example.com", comment.email)
  end

  private

  def create_comment(name)
    comment = Comment.new(post: @post,
                          email: "example@example.com",
                          name: name,
                          body: "<a href='https://example.com'>Some text</a>")
    comment.save
  end
end
