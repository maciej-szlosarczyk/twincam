require File.expand_path("model_test_helper", __dir__)
require File.expand_path("../../lib/features", __dir__)

class FeaturesTest < TwinCamModelTest
  def setup
    super

    @previous_pagination = ENV["TWIN_CAM_PAGINATION"]
    @previous_spam_timeout = ENV["TWIN_CAM_SPAM_TIMEOUT"]
    @previous_google_site_verification = ENV["GOOGLE_SITE_VERIFICATION"]
  end

  def teardown
    super

    ENV["TWIN_CAM_PAGINATION"] = @previous_pagination
    ENV["TWIN_CAM_SPAM_TIMEOUT"] = @previous_spam_timeout
    ENV["GOOGLE_SITE_VERIFICATION"] = @previous_google_site_verification
  end

  def test_pagination_step
    ENV["TWIN_CAM_PAGINATION"] = "1"

    assert_equal(1, Features.pagination_step)
  end

  def test_spam_timeout
    ENV["TWIN_CAM_SPAM_TIMEOUT"] = "1"

    assert_equal(1, Features.spam_timeout)
  end

  def test_google_site_verification
    ENV["GOOGLE_SITE_VERIFICATION"] = "Google 123"
    assert_equal("Google 123", Features.google_site_verification)
  end
end
