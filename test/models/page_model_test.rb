# frozen_string_literal: true

require File.expand_path("model_test_helper", __dir__)

# Model tests for Page
class PageModelTest < TwinCamModelTest
  def setup
    super

    @admin = Fabricate(:administrator)
  end

  def teardown
    super
  end

  def test_creates_universally_accepted_slugs_for_ascii
    page = Page.new(user: @admin, title: "Simple title")
    page.save
    assert_equal("simple-title", page.slug)
  end

  def test_url_path_returns_nil_if_not_persisted
    page = Page.new(user: @admin, title: "Simple title")
    refute(page.url_path)
  end

  def test_author_returns_user_instance
    page = Page.new(user: @admin, title: "Simple title")

    assert_equal(@admin, page.author)
  end

  def test_url_path_returns_just_slug
    page = Page.new(user: @admin, title: "Simple title")
    page.save
    assert_equal("simple-title", page.url_path)
  end

  def test_creates_universally_accepted_slugs_for_polish_and_estonian
    page = Page.new(user: @admin, title: "Chrząszcz Õllekäpp")
    page.save
    assert_equal("chrz-szcz--llek-pp", page.slug)
  end

  def test_creates_only_unique_slugs
    page = Page.new(user: @admin, title: "Simple title")
    other_page = Page.new(user: @admin, title: "Simple title")
    page.save
    other_page.save
    assert_equal("simple-title", page.slug)
    assert_equal("simple-title-1", other_page.slug)
  end
end
