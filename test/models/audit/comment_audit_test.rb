# frozen_string_literal: true

require File.expand_path("../model_test_helper", __dir__)

class CommentAuditTest < TwinCamModelTest
  def setup
    super

    Timecop.freeze("2018-04-01 11:00 +0000")

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
    @comment = Fabricate(:first_comment, post_id: @post.id)
  end

  def teardown
    super

    Timecop.return
  end

  def test_inserting_comment_creates_new_object
    query = "SELECT * FROM audit.comments where action = 'INSERT' " \
            "and object_id = #{@comment.id}"
    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal(@comment.body, audit_json_new_value[:body])
  end

  def test_updating_comment_creates_new_object
    @comment.update(body: "Foo")
    query = "SELECT * FROM audit.comments where action = 'UPDATE' " \
            "and object_id = #{@comment.id}"

    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal("Foo", audit_json_new_value[:body])
  end

  def test_deleting_comment_creates_new_object
    @comment.delete
    query = "SELECT * FROM audit.comments where action = 'DELETE' " \
            "and object_id = #{@comment.id}"
    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal({}, audit_json_new_value)
  end
end
