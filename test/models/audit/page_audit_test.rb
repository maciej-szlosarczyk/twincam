# frozen_string_literal: true

require File.expand_path("../model_test_helper", __dir__)

class PageAuditTest < TwinCamModelTest
  def setup
    super

    Timecop.freeze("2018-04-01 11:00 +0000")

    user = Fabricate(:administrator)
    @page = Fabricate(:second_page, author_id: user.id)
  end

  def teardown
    super

    Timecop.return
  end

  def test_inserting_page_creates_new_object
    data_object = DB["SELECT * FROM audit.pages where action = 'INSERT'"].first

    assert_equal(@page.id, data_object[:object_id])
  end

  def test_updating_page_creates_new_object
    @page.update(title: "Foo")
    data_object = DB["SELECT * FROM audit.pages where action = 'UPDATE'"].first

    assert_equal(@page.id, data_object[:object_id])

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal("Foo", audit_json_new_value[:title])
  end

  def test_deleting_page_creates_new_object
    @page.delete
    data_object = DB["SELECT * FROM audit.pages where action = 'DELETE'"].first

    assert_equal(@page.id, data_object[:object_id])

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal({}, audit_json_new_value)
  end
end
