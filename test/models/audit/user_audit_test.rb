# frozen_string_literal: true

require File.expand_path("../model_test_helper", __dir__)

class UserAuditTest < TwinCamModelTest
  def setup
    super

    Timecop.freeze("2018-04-01 11:00 +0000")

    @params = {
      email: "test@email.com",
      password: "P2ssword",
      name: "Test Admin"
    }
  end

  def teardown
    super

    Timecop.return
  end

  def test_creating_a_user_record_creates_audit
    user = User.new(@params)
    user.save
    audit_log = DB["SELECT * FROM audit.users WHERE action = 'INSERT'"].first

    audit_json_new_value = JSON.parse(
      audit_log[:new_value],
      symbolize_names: true
    )

    assert_equal(@params[:email], audit_json_new_value[:email])
    assert_equal(@params[:name], audit_json_new_value[:name])
  end

  def test_updating_a_user_record_creates_audit
    user = User.new(@params)
    user.save
    user.update(email: "foo@bar.baz")
    audit_log = DB["SELECT * FROM audit.users WHERE action = 'UPDATE'"].first
    audit_json_new_value = JSON.parse(
      audit_log[:new_value],
      symbolize_names: true
    )

    assert_equal("foo@bar.baz", audit_json_new_value[:email])
    assert_equal(@params[:name], audit_json_new_value[:name])
  end

  def test_deleting_a_user_record_creates_audit
    user = User.new(@params)
    user.save
    user.delete
    audit_log = DB["SELECT * FROM audit.users WHERE action = 'DELETE'"].first
    audit_json_new_value = JSON.parse(
      audit_log[:new_value],
      symbolize_names: true
    )

    assert_equal({}, audit_json_new_value)
  end
end
