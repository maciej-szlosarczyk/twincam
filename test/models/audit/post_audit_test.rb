# frozen_string_literal: true

require File.expand_path("../model_test_helper", __dir__)

class PostAuditTest < TwinCamModelTest
  def setup
    super

    Timecop.freeze("2018-04-01 11:00 +0000")

    user = Fabricate(:administrator)
    @post = Fabricate(:hello_world_post, author_id: user.id)
  end

  def teardown
    super

    Timecop.return
  end

  def test_inserting_post_creates_new_object
    query = "SELECT * FROM audit.posts where action = 'INSERT' " \
            "and object_id = #{@post.id}"
    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal(@post.body, audit_json_new_value[:body])
  end

  def test_updating_post_creates_new_object
    @post.update(body: "Foo")
    query = "SELECT * FROM audit.posts where action = 'UPDATE' " \
            "and object_id = #{@post.id}"
    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal("Foo", audit_json_new_value[:body])
  end

  def test_deleting_post_creates_new_object
    @post.delete
    query = "SELECT * FROM audit.posts where action = 'DELETE' " \
            "and object_id = #{@post.id}"
    data_object = DB[query].first

    audit_json_new_value = JSON.parse(
      data_object[:new_value],
      symbolize_names: true
    )

    assert_equal({}, audit_json_new_value)
  end
end
