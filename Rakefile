# frozen_string_literal: true

require File.expand_path("lib/application_logger", __dir__)

# Migrate
migrate = lambda do |env, version|
  ENV["RACK_ENV"] = env
  require File.expand_path("lib/db", __dir__)
  require "logger"
  Sequel.extension :migration
  DB.loggers << ApplicationLogger::StandardOut.logger
  Sequel::Migrator.apply(DB, "migrate", version)
end

# Migrations
# rubocop:disable Metrics/BlockLength
namespace :db do
  namespace :dev do
    desc "Migrate development database all the way down"
    task :down do
      migrate.call("development", 0)
    end

    desc "Migrate development database all the way up"
    task :up do
      migrate.call("development", nil)
    end

    desc "Migrate development database down, and then up"
    task :bounce do
      migrate.call("development", 0)
      Sequel::Migrator.apply(DB, "migrate")
    end
  end

  namespace :test do
    desc "Migrate test database all the way down"
    task :down do
      migrate.call("test", 0)
    end

    desc "Migrate test database all the way up"
    task :up do
      migrate.call("test", nil)
    end

    desc "Migrate test database down, and then up"
    task :bounce do
      migrate.call("test", 0)
      Sequel::Migrator.apply(DB, "migrate")
    end
  end

  desc "Apply migrations in production database"
  task :prod do
    migrate.call("production", nil)
  end
end
# rubocop:enable Metrics/BlockLength

# Shell
pry = proc do |env|
  ENV["RACK_ENV"] = env
  cmd = "pry"
  sh "#{cmd} -r ./lib/models -r ./lib/contracts"
end

namespace :pry do
  desc "Open pry in dev environment"
  task :dev do
    pry.call("development")
  end

  desc "Open pry in test environment"
  task :test do
    pry.call("test")
  end

  desc "Open pry in production environment"
  task :production do
    pry.call("production")
  end
end

desc "Open pry in dev environment"
task pry: "pry:dev"
